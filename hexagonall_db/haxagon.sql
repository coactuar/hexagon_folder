-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 22, 2022 at 07:35 AM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `haxagon`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pollanswers`
--

CREATE TABLE `tbl_pollanswers` (
  `id` int(11) NOT NULL,
  `poll_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `poll_answer` varchar(10) NOT NULL,
  `poll_at` datetime NOT NULL,
  `points` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pollanswers`
--

INSERT INTO `tbl_pollanswers` (`id`, `poll_id`, `users_id`, `poll_answer`, `poll_at`, `points`) VALUES
(1, 2, 9, 'opt2', '2020-09-21 00:41:38', 1),
(2, 1, 9, 'opt2', '2020-09-21 00:46:09', 1),
(3, 3, 11, 'opt3', '2020-09-21 02:30:48', 0),
(4, 4, 11, 'opt2', '2020-09-21 02:39:49', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_polls`
--

CREATE TABLE `tbl_polls` (
  `id` int(11) NOT NULL,
  `poll_question` varchar(500) NOT NULL,
  `poll_opt1` varchar(500) NOT NULL,
  `poll_opt2` varchar(500) NOT NULL,
  `poll_opt3` varchar(500) NOT NULL,
  `poll_opt4` varchar(500) NOT NULL,
  `correct_ans` varchar(10) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `poll_over` int(11) NOT NULL DEFAULT '0',
  `show_results` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_polls`
--

INSERT INTO `tbl_polls` (`id`, `poll_question`, `poll_opt1`, `poll_opt2`, `poll_opt3`, `poll_opt4`, `correct_ans`, `active`, `poll_over`, `show_results`) VALUES
(1, 'test', '1', '2', '3', '4', 'opt2', 0, 0, 0),
(2, 'Test2', '12', '21', '32', '213', 'opt2', 0, 0, 0),
(3, 'Test3', 'test', '2', '123', '3', 'opt1', 0, 0, 0),
(4, 'How are you ?', 'Fine', 'Good', 'Bad', 'Worse', 'opt1', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `user_empid` int(11) NOT NULL,
  `user_question` varchar(200) NOT NULL,
  `asked_at` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_questions`
--

INSERT INTO `tbl_questions` (`id`, `user_name`, `user_empid`, `user_question`, `asked_at`) VALUES
(1, '', 0, 'test', '2020/09/20 15:36:40'),
(2, '', 0, 'test', '2020/09/20 15:38:17'),
(3, '', 0, 'test', '2020/09/20 15:41:21'),
(4, '', 0, 'test', '2020/09/20 15:43:12'),
(5, '', 0, 'test', '2020/09/20 15:43:54'),
(6, '', 0, 'test', '2020/09/20 15:44:36'),
(7, 'Reynold Roy', 0, 'test', '2020/09/20 15:47:26');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `mobile` varchar(200) NOT NULL,
  `accode` varchar(200) NOT NULL,
  `login_date` varchar(200) NOT NULL,
  `logout_date` varchar(200) NOT NULL,
  `joining_date` varchar(200) NOT NULL,
  `logout_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `name`, `mobile`, `accode`, `login_date`, `logout_date`, `joining_date`, `logout_status`) VALUES
(1, 'Reynold Roy', '123', '123', '2020/09/20 15:32:36', '2020/09/20 15:33:06', '2020/09/20 15:32:36', 1),
(2, 'Reynold Roy', '123', '123', '2020/09/20 16:19:29', '2020/09/20 16:19:59', '2020/09/20 16:19:29', 1),
(3, 'Reynold Roy', '123', '123', '2020/09/20 16:59:42', '2020/09/20 17:00:12', '2020/09/20 16:59:42', 1),
(4, 'viraj', '9765875731', '11', '2020/09/20 17:05:37', '2020/09/20 17:06:07', '2020/09/20 17:05:37', 1),
(5, 'viraj', '9765875731', '11', '2020/09/20 17:07:28', '2020/09/20 17:07:58', '2020/09/20 17:07:28', 1),
(6, 'Pooja', '9768161921', '1234', '2020/09/20 17:12:03', '2020/09/20 17:12:33', '2020/09/20 17:12:03', 1),
(7, 'Sujatha ', '9845563760', '12345', '2020/09/20 17:17:17', '2020/09/20 17:17:47', '2020/09/20 17:17:17', 1),
(8, 'Reynold', '123', '123', '2020/09/20 17:25:35', '2020/09/20 17:26:05', '2020/09/20 17:25:35', 1),
(9, 'PAWAN SHILWANT', '9545329222', '111111', '2020/09/20 18:02:00', '2020/09/20 18:02:30', '2020/09/20 18:02:00', 1),
(10, 'Sujatha ', '9845563760', '12345', '2020/09/20 18:37:44', '2020/09/20 18:38:14', '2020/09/20 18:37:44', 1),
(11, 'PAWAN SHILWANT', '9545329222', '11111', '2020/09/20 18:38:59', '2020/09/20 18:39:29', '2020/09/20 18:38:59', 1),
(12, 'Reynold Roy', '123', '123', '2020/09/20 20:18:55', '2020/09/20 20:19:25', '2020/09/20 20:18:55', 1),
(13, 'viraj', '9765875731', '22', '2020/09/20 20:24:13', '2020/09/20 20:24:43', '2020/09/20 20:24:13', 1),
(14, 'Amit', '9029392185', '123456', '2020/09/20 20:43:04', '2020/09/20 20:43:34', '2020/09/20 20:43:04', 1),
(15, 'viraj', '9765875731', '22', '2020/09/20 20:44:52', '2020/09/20 20:45:22', '2020/09/20 20:44:52', 1),
(16, 'Rahul', '9820133946', '123456', '2020/09/20 20:47:56', '2020/09/20 20:48:26', '2020/09/20 20:47:56', 1),
(17, 'PAWAN SHILWANT', '09545329222', '11111', '2020/09/20 20:48:25', '2020/09/20 20:48:55', '2020/09/20 20:48:25', 1),
(18, 'Rahul', '9820133946', '123456', '2020/09/20 20:50:43', '2020/09/20 20:51:13', '2020/09/20 20:50:43', 1),
(19, 'Amit', '8767402044', '12345', '2020/09/20 20:51:36', '2020/09/20 20:52:06', '2020/09/20 20:51:36', 1),
(20, 'Rahul', '9820133946', '123456', '2020/09/20 20:52:08', '2020/09/20 20:52:38', '2020/09/20 20:52:08', 1),
(21, 'Rahul', '9820133946', '12345', '2020/09/20 20:56:58', '2020/09/20 20:57:28', '2020/09/20 20:56:58', 1),
(22, 'PAWAN SHILWANT', '09545329222', '11111', '2020/09/20 21:00:04', '2020/09/20 21:00:34', '2020/09/20 21:00:04', 1),
(23, 'Sujatha ', '9845563760', '12345', '2020/09/20 21:49:33', '2020/09/20 21:50:03', '2020/09/20 21:49:33', 1),
(24, 'Pooja', '9768161921', '12345', '2020/09/25 16:50:30', '2020/09/25 16:51:00', '2020/09/25 16:50:30', 1),
(639, 'Viraj', '9765875731', '369', '2020/10/08 10:12:50', '2020/10/08 10:18:00', '2020/10/08 10:12:50', 1),
(640, 'v', '9765875731', '369', '2020/10/08 10:19:49', '2020/10/08 10:32:23', '2020/10/08 10:19:49', 1),
(641, 'Viraj', '7786788', '369', '2020/10/08 11:15:49', '2020/10/08 12:44:43', '2020/10/08 11:15:49', 1),
(642, 'test', '09611518064', '369', '2020/11/12 14:37:32', '2020/11/12 14:38:03', '2020/11/12 14:37:32', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_pollanswers`
--
ALTER TABLE `tbl_pollanswers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_polls`
--
ALTER TABLE `tbl_polls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_pollanswers`
--
ALTER TABLE `tbl_pollanswers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_polls`
--
ALTER TABLE `tbl_polls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=643;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
