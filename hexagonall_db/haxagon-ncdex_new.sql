-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 22, 2022 at 07:35 AM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `haxagon-ncdex_new`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pollanswers`
--

CREATE TABLE `tbl_pollanswers` (
  `id` int(11) NOT NULL,
  `poll_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `poll_answer` varchar(10) NOT NULL,
  `poll_at` datetime NOT NULL,
  `points` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_polls`
--

CREATE TABLE `tbl_polls` (
  `id` int(11) NOT NULL,
  `poll_question` varchar(500) NOT NULL,
  `poll_opt1` varchar(500) NOT NULL,
  `poll_opt2` varchar(500) NOT NULL,
  `poll_opt3` varchar(500) NOT NULL,
  `poll_opt4` varchar(500) NOT NULL,
  `correct_ans` varchar(10) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `poll_over` int(11) NOT NULL DEFAULT '0',
  `show_results` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `user_question` varchar(200) NOT NULL,
  `asked_at` varchar(200) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_questions`
--

INSERT INTO `tbl_questions` (`id`, `user_name`, `user_email`, `user_question`, `asked_at`, `speaker`, `answered`) VALUES
(1, 'Akshat Jharia', 'akshatjharia@gmail.com', 'Hello', '2021/03/24 08:55:07', 0, 0),
(3, 'NIshanth S', '', 'Hello', '2021/03/24 10:46:43', 0, 0),
(4, 'NIshanth S', '', 'HI', '2021/03/24 11:03:31', 0, 0),
(5, 'NIshanth S', '', 'Hello ', '2021/03/24 11:18:04', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_reactions`
--

CREATE TABLE `tbl_reactions` (
  `id` int(11) NOT NULL,
  `userid` varchar(255) NOT NULL,
  `reaction` varchar(50) NOT NULL,
  `reaction_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `fname` varchar(200) NOT NULL,
  `lname` varchar(200) NOT NULL,
  `mobile` varchar(200) NOT NULL,
  `occupation` varchar(200) NOT NULL,
  `login_date` varchar(200) DEFAULT NULL,
  `logout_date` varchar(200) DEFAULT NULL,
  `joining_date` varchar(200) NOT NULL,
  `logout_status` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `city` varchar(200) NOT NULL,
  `state` varchar(200) NOT NULL,
  `country` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `fname`, `lname`, `mobile`, `occupation`, `login_date`, `logout_date`, `joining_date`, `logout_status`, `email`, `city`, `state`, `country`) VALUES
(1, 'NIshanth', 'S', '8747973536', 'Ops', '2021/03/24 15:27:54', '2021/03/24 15:28:24', '2021/03/24 11:15:41', 0, 'nishanth@coact.co.in', 'Coact', 'karnataka', 'Bangalore'),
(2, 'Akshat', 'Jharia', '7204420017', '', '2021/04/09 14:54:24', '2021/04/09 16:38:37', '2021/03/24 11:24:12', 1, 'akshatjharia@gmail.com', 'Bangalore', 'Karnataka', 'India'),
(3, 'Akshat', 'Jharia', '7204420018', '', NULL, '2021/04/09 16:38:37', '2021/03/24 11:34:00', 0, 'akshatjharia@gmail.com', 'Bangalore', 'Karnataka', 'India'),
(4, 'NIshanth', 'S', '9728130384', 'Ops', NULL, NULL, '2021/03/24 11:36:08', 0, 'nishu_8989@yahoo.com', 'Coact', 'karnataka', 'Bangalore'),
(5, 'Saakshi', 'Bedi', '9796714478', 'Client Servicing', '2021/03/24 13:57:07', '2021/03/24 13:58:07', '2021/03/24 13:49:41', 0, 'saakshi@hexagonevents.com', 'Hexagon Events', 'Maharashtra', 'Mumbai'),
(6, 'Purav', 'Gandhi', '9702858011', 'Operation', '2021/08/24 15:35:39', '2021/08/24 15:36:39', '2021/03/24 13:53:27', 1, 'purav@hexagonevents.com', 'Hexagon Events', 'Maharashtra', 'Mumbai'),
(7, 'Amit', 'Parkar', '9029392185', 'Manager', '2021/08/13 16:34:00', '2021/08/13 16:35:00', '2021/03/24 13:55:32', 1, 'amit@hexagonevents.com', 'Hexagon Events', 'Maharashtra', 'Mumbai'),
(8, 'Akshat', 'Jharia', '131546', '', NULL, '2021/04/09 16:38:37', '2021/03/24 13:57:41', 0, 'akshatjharia@gmail.com', '', 'Karnataka', 'India'),
(9, 'Akshat', 'Jharia', '17204420017', '', NULL, '2021/04/09 16:38:37', '2021/03/24 13:58:19', 0, 'akshatjharia@gmail.com', 'Bangalore', 'Karnataka', 'India'),
(10, 'neeraj', 'reddy', '9700467764', '9700467764', '2022/01/10 15:57:20', '2022/01/10 15:58:08', '2021/03/24 14:04:22', 0, 'neeraj@coact.co.in', 'nellore', 'ap', 'india'),
(11, 'Kalpesh', 'sheth', '09820305936', 'NCDEX', '2021/03/24 14:25:45', '2021/03/24 14:26:45', '2021/03/24 14:11:52', 0, 'kalpesh.sheth@ncdex.com', 'MUMBAI', 'MAHARASHTRA', 'India'),
(12, 'Rahul', 'Soni', '9820133946', '', '2021/03/27 11:33:24', '2021/03/27 11:33:46', '2021/03/24 14:13:18', 0, 'rahulsoni@hexagonevents.com', 'Hexagon', 'Maharashtra', 'Mumbai'),
(13, 'Amit', 'Parkar', '9819488368', '', '2021/03/24 14:21:54', '2021/03/24 14:22:54', '2021/03/24 14:21:13', 0, 'amit.parkar91@gmail.com', 'Hexagon', 'Maharashtra', 'Mumbai'),
(14, 'Ashish', 'Dube', '8291249073', 'Service', NULL, NULL, '2021/03/24 14:35:54', 0, 'ashish.dube@ncdex.com', 'NCDEX', 'Maharashtra', 'Mumbai'),
(15, 'NIshanth', 'S', '7896541258', 'Ops', NULL, NULL, '2021/03/24 15:26:49', 0, 'knvewoicalks@gmail.com', 'bangalore', 'karnataka', 'Bangalore'),
(16, 'Pradeep', 'Chindarkar', '9773694979', 'Private Job', NULL, NULL, '2021/03/24 16:29:16', 0, 'pradeep.chindarkar@ncdex.com', 'NCDEX', 'Maharashtra', 'Mumbai'),
(17, 'shrikant', 'kuwalekar', '09920437473', 'FarmSpeak', NULL, NULL, '2021/03/24 16:48:05', 0, 'ksrikant10@gmail.com', 'mumbai', 'Maharashtra', 'India'),
(18, 'Amit', 'Upadhyay', '08879200982', 'Service', NULL, NULL, '2021/03/24 16:49:53', 0, 'amitsupadhyay@rediffmail.com', 'Thane', 'Maharashtra', 'India'),
(19, 'Nusrat', 'Khan', '9172498643', 'Service', '2021/03/26 15:57:09', '2021/03/26 15:58:09', '2021/03/24 17:22:07', 0, 'nusrat.khan@ncdex.com', 'NCDEX', 'Maharashtra', 'Mumbai'),
(20, 'Kavita ', 'Jha ', '9930267919', 'VP', NULL, NULL, '2021/03/24 17:42:54', 0, 'Kavita.jha@ncdex.com', 'NCDEX', 'Maharashtra', 'India'),
(21, 'Sanjeev', 'Chauhan', '9828383222', 'Service Ind.', '2021/03/26 15:31:19', '2021/03/26 15:32:19', '2021/03/24 18:54:11', 0, 'sanjeev.chauhan@ncdex.com', 'NCDEX', 'Raj', 'Jaipur'),
(22, 'Poornima ', 'Shetty', '9167448895', 'Service', '2021/03/26 17:46:57', '2021/03/26 17:56:50', '2021/03/24 19:47:36', 0, 'poornima.shetty@ncdex.com', 'Andheri (East)', 'MAHARASHTRA', 'India'),
(23, 'Preshita', 'Prabhu', '9769392916', '', '2021/03/26 15:34:10', '2021/03/26 15:35:10', '2021/03/24 22:10:51', 0, 'preshita.prabhu@ncdex.com', 'NCDEX', 'Maharashtra', 'Mumbai'),
(24, 'Sumit', 'Chahal', '9653919198', '', NULL, NULL, '2021/03/25 06:28:36', 0, 'sumit@ncdex.com', '', 'Rajasthan', 'Jaipur'),
(25, 'Aleen', 'Mukherjee', '09811154218', '', '2021/03/26 17:50:47', '2021/03/26 17:51:47', '2021/03/25 08:40:24', 0, 'aleen.mukherjee@ncdex.com', 'New Delhi', 'Select State', 'India'),
(26, 'Srinivasa Rao', 'Adapala', '9618426548', 'MT', '2021/03/26 15:29:39', '2021/03/26 15:30:39', '2021/03/25 08:49:23', 0, 'adalala.rao@ncdex.com', 'NCDEX', 'Andhra Pradesh', 'Addanki'),
(27, 'Kaustubh', 'Mundada', '09920240906', '', '2021/03/26 15:33:05', '2021/03/26 15:34:05', '2021/03/25 09:17:11', 0, 'kaustubh.mundada@ncdex.com', 'Kalyan', 'Maharashtra', 'India'),
(28, 'Harshit', 'Kabra', '8118815090', 'Trading Operations', NULL, NULL, '2021/03/25 09:36:21', 0, 'harshit.kabra@ncdex.com', 'NCDEX', 'Rajasthan', 'Jaipur'),
(29, 'Adhish ', 'Singhal', '9811798498', 'Service', '2021/03/26 17:38:51', '2021/03/26 17:39:51', '2021/03/25 09:38:15', 0, 'adhish.singhal@ncdex.com', 'NCDEX', 'Delhi', 'Delhi'),
(30, 'Brajesh', 'Sisodia', '9928817076', 'Service', '2021/03/26 15:40:15', '2021/03/26 15:41:15', '2021/03/25 09:53:52', 0, 'Brajesh.sisodia@ncdex.com', 'NCDEX LTD', 'Rajasthan', 'jaipur'),
(31, 'BHUWAN', 'BHASKAR', '9560473332', 'jOB', NULL, NULL, '2021/03/25 09:59:04', 0, 'bhuwan.bhaskar@ncdex.com', 'NCDEX', 'DELHI', 'NEW DELHI'),
(32, 'deepak', 'gupta', '9982407806', 'service', '2021/03/26 17:16:29', '2021/03/26 17:17:29', '2021/03/25 10:01:39', 0, 'deepak.gupta@ncdex.com', 'ncdex', 'rajasthan', 'sriganganagar'),
(33, 'Harish', 'Parmar', '9427245561', 'Vyapar', NULL, NULL, '2021/03/25 10:09:05', 0, 'parmarharish74@gmailcom', 'Self business', 'Gujarat', 'Devbhumi Dwarka'),
(34, 'Bheenya Ram', 'Chaudhary', '9414530172', '', NULL, NULL, '2021/03/25 10:15:38', 0, 'brsaran.saran072@gmail.com', 'Shri chamunda krishi sewa kendra', 'Rajasthan', 'Gudamalani'),
(35, 'Rameshwar', 'Rai', '8889912120', 'Service', '2021/03/26 15:57:58', '2021/03/26 15:58:58', '2021/03/25 10:20:16', 0, 'rameshwar.rai@ncdex.com', 'NCDEX Ltd', 'MP', 'Indore'),
(36, 'Sujatha ', 'Natesh', '9845563760', '', '2021/03/25 10:31:08', '2021/03/25 11:03:43', '2021/03/25 10:30:39', 0, 'sujatha@coact.co.in', 'Bangalore', 'Karnataka', 'india'),
(37, 'Shamal', 'Kadam', '9920739775', 'Executive', '2021/03/26 17:20:28', '2021/03/26 17:32:53', '2021/03/25 10:32:06', 0, 'shamal.kadam@ncdex.com', 'NCDEX', 'Maharashtra', 'MUmbai'),
(38, 'Amit', 'Sahita', '9820138115', 'Business', NULL, NULL, '2021/03/25 10:45:44', 0, 'amitmsahita@gmail.con', 'Amit Sahita', 'Maharashtra', 'Mumbai'),
(39, 'PAWAN', 'SHILWANT', '9544329222', 'XX', NULL, NULL, '2021/03/25 10:46:21', 0, 'pawan@coact.co.in', 'CXX', 'Maharashtra ', 'Mumbai '),
(40, 'VIRAL', 'Shah', '9820583080', 'Service', NULL, NULL, '2021/03/25 10:47:02', 0, 'viral.shah@iiflw.com', 'IIFLW', 'Maharashtra', 'Mumbai'),
(41, 'Suresh', 'Kumar', '19810798153', 'Trading and Risk Management ', NULL, NULL, '2021/03/25 10:49:04', 0, 'suresh_kumar@cargill.com', 'Gurgaon', 'India', 'India'),
(42, 'NAWAL', 'JAIN', '9921560004', 'BROKER', NULL, NULL, '2021/03/25 10:50:40', 0, 'MAIL.JCCAKOLA@GMAIL.COM', 'JAIN CANVASSING COMPANY', 'MAHARASHTRA', 'AKOLA'),
(43, 'Manoj Kumar ', 'Jain ', '7389934305', 'Director Head Commodity Research ', '2021/03/26 16:59:53', '2021/03/26 17:56:48', '2021/03/25 10:53:35', 0, 'manoj.jain@prithvifinmart.com', 'Prithvi Finmart Pvt Ltd ', 'Madhya Pradesh', 'Indore '),
(44, 'AMIT ', 'BHOJANI', '9426994162', 'Agri Traders', NULL, NULL, '2021/03/25 10:55:44', 0, 'amit.ambajiagro@gmail.com', 'Ambajji Agro Product Industries', 'Gujarat', 'Rajkot'),
(45, 'Amar', 'Singh', '9324543309', 'Service ', '2021/03/26 17:13:37', '2021/03/26 17:14:37', '2021/03/25 10:58:20', 0, 'amar.singh@angelbroking.com', 'Angel Broking Ltd.', 'Maharashtra', 'Mumbai '),
(46, 'Ashutosh', 'Deshpande', '9619207794', 'Head of Value Chains', '2021/03/26 15:05:36', '2021/03/26 15:06:36', '2021/03/25 10:58:38', 0, 'ashutosh.deshpande@reliancefoundation.org', 'Reliance Foundation', 'Maharashtra', 'Thane'),
(47, 'Ketan', 'Shah', '982041072', 'Service ', NULL, NULL, '2021/03/25 10:59:49', 0, 'ketan@angelbroking.com', 'Angel Broking Ltd.', 'Maharashtra', 'Mumbai '),
(48, 'Ajay', 'Kedia', '9320096333', 'Broker', NULL, NULL, '2021/03/25 11:03:32', 0, 'info@kediacapital.com', 'Kedia Capital', 'Maharashtra', 'Mumbai'),
(49, 'Amit', 'Pujara', '9819234545', 'Service', '2021/03/26 15:41:24', '2021/03/26 17:56:58', '2021/03/25 11:07:49', 0, 'amit.pujara@nerlindia.com', 'National E-Repository Limited', 'Maharashtra', 'Mumbai'),
(50, 'Sudhir', 'Guptta', '8099049116', 'NCDEX e Markets Ltd', NULL, NULL, '2021/03/25 11:16:16', 0, 'sudhir.gupta@neml.in', 'Hyderabad', 'Telangana', 'India'),
(51, 'SUBHRANIL', 'DEY', '9968624802', 'SR. RESEARCH ASSOCIATE', NULL, NULL, '2021/03/25 11:18:01', 0, 'subhranildey@smcindiaonline.com', 'NEW DELHI', 'Delhi (UT)', 'India'),
(52, 'Vijay', 'Joshi', '9879001683', 'Owner ', '2021/03/26 15:29:46', '2021/03/26 15:30:46', '2021/03/25 11:31:21', 0, 'vijayjoshiunjha@gmail.com', 'Vijay Exim', 'Gujarat ', 'Unjha '),
(53, 'Vedika', 'Narvekar', '7045278805', 'Research Analyst', NULL, NULL, '2021/03/25 11:33:22', 0, 'vedikanarvekar@rathi.com', 'Anand Rathi Shares and Stock Brokers Limited', 'Maharasthra', 'Mumbai'),
(54, 'Kalpesh', 'Rathi', '9828849999', 'Professional', '2021/03/26 15:32:20', '2021/03/26 15:33:20', '2021/03/25 11:57:07', 0, 'kalpesh@9star.in', 'Jodhpur', 'Rajasthan', 'Jodhpur'),
(55, 'Chetan ', 'Dabke', '8050530705', 'Director', '2021/03/26 16:38:34', '2021/03/26 16:39:34', '2021/03/25 12:00:27', 0, 'md.chetan@gmail.com', 'Bidar FPO', 'KARNATAKA', 'BIDAR'),
(56, 'Ravindra', 'Rao', '9820179347', 'Service', NULL, NULL, '2021/03/25 12:47:54', 0, 'ravindra.rao@kotak.com', 'Kotak securities Pvt Ltd', 'Maharashtra ', 'Mumbai'),
(57, 'KHALID', 'KHAN', '9959629444', 'HEAD - COMMODITIES AND SUPPLY CHAIN', '2021/03/26 15:31:18', '2021/03/26 17:26:59', '2021/03/25 13:07:13', 0, 'KHALID.KHAN@ITC.IN', 'ITC LIMITED', 'ANDHRA PRADESH', 'GUNTUR'),
(58, 'Praveen', 'Agrawal', '9887009551', 'Business', NULL, NULL, '2021/03/25 14:38:45', 0, 'pravnagrawal@gmail.com', 'Kota Agro Protiens Pvt Ltd', 'Rajasthan', 'Kota'),
(59, 'shailesh', 'agrawal', '9893038004', 'business', '2021/03/26 15:36:58', '2021/03/26 15:37:58', '2021/03/25 14:56:08', 0, 'agrshailesh@gmail.com', 'Essence commodities pvt ltd', 'mp', 'indore'),
(60, 'Kapil', 'dev', '09999277284', 'NCDEX', NULL, NULL, '2021/03/25 15:57:18', 0, 'kapil.dev@ncdex.com', 'Mumbai', 'Maharashtra', 'India'),
(61, 'ABHISHEK', 'RAI', '09167012727', '', '2021/03/25 16:04:40', '2021/03/25 16:05:40', '2021/03/25 15:58:08', 0, 'abhishek.rai@nerlindia.com', 'Thane', 'Maharashtra', 'India'),
(62, 'Arvind', 'Jha', '9769689943', 'Service', '2021/03/26 15:31:22', '2021/03/26 16:50:17', '2021/03/25 16:00:14', 0, 'arvind.jha@angelbroking.com', 'Angel Broking Ltd', 'Maharashtra', 'Mumbai'),
(63, 'Dnyaneshwar', 'Tayde', '7888038163', 'Farmer', NULL, NULL, '2021/03/25 16:08:10', 0, 'taydednyaneshwar@gmail.com', 'sankat  mochan', 'Maharashtra', 'washim'),
(64, 'Bhagyashree', 'Biwalkar', '9819645352', '', '2021/03/25 16:31:27', '2021/03/25 16:32:27', '2021/03/25 16:30:06', 0, 'bhagyashreebiwalkar2020@gmail.com', 'Ncdex', 'Maharashtra', 'Kalyan'),
(65, 'abhishek', 'chauhan', '9930997169', 'service', '2021/03/26 15:55:41', '2021/03/26 15:56:41', '2021/03/25 16:30:17', 0, 'abhishek.chouhan@swastika.co.in', 'Swastika', 'mp', 'indore'),
(66, 'Om Prakash', 'Singh', '9323457900', 'Service', NULL, NULL, '2021/03/25 16:48:21', 0, 'omprakashsingh@rathi.com', 'Anandrathi shares and Brokers Ltd', 'Maharastra', 'Mumbai'),
(67, 'NARENDER', 'KUMAR', '9772078777', 'SHARES & COMMODITY MAMBER', '2021/03/26 15:33:06', '2021/03/26 15:34:06', '2021/03/25 16:49:19', 0, 'OFFICE@SBMCPL.CO.IN', 'SHREE BALAJI MULTICOMMODITIES PVT. LTD.', 'RAJASTHAN', 'SRIGANGANAGAR'),
(68, 'Anuj', 'Gupta', '9899456241', 'Commodity Broker', '2021/03/26 15:32:25', '2021/03/26 15:33:25', '2021/03/25 17:13:39', 0, 'gupta.anuj@iifl.com', 'IIFL SECURITUES', 'Maharashtra', 'Mumbai'),
(69, 'Manoj', 'Khandelwal', '9413340830', 'Agri business', NULL, NULL, '2021/03/25 17:21:06', 0, 'ambicajpr@gmail.com', 'Ambica industries', 'Raj', 'Jaipur'),
(70, 'Manu', 'Shankar', '09811908911', '', NULL, NULL, '2021/03/25 17:38:59', 0, 'shankar.manu@gmail.com', 'New Delhi', 'Delhi', 'India'),
(71, 'Dinesh', 'Garg', '7738898394', '', '2021/03/25 20:20:39', '2021/03/25 20:21:39', '2021/03/25 20:19:31', 0, 'dinesh.garg@nirmalbang.com', 'Nirmal Bang Securities Pvt Ltd', 'Maharashtra', 'Mumbai'),
(72, 'Amit', 'Maheshwari', '7045940220', 'Service', NULL, NULL, '2021/03/25 20:27:53', 0, 'amit.maheshwari@etgworld.com', 'Etg ', 'Maharashtrs', 'Mumvai'),
(73, 'Gopal', 'Joshi', '9320538222', 'AVP', NULL, NULL, '2021/03/25 20:31:53', 0, 'gopal.joshi@iifl.com', 'IiFL Securities Ltd', 'Maharashtra', 'Mumbai'),
(74, 'Bulesh', 'Bhageria', '9322415899', '', '2021/03/26 16:28:17', '2021/03/26 16:50:47', '2021/03/25 20:42:01', 0, 'buleshb@gmail.com', 'Anand rathi', 'Maharashtra ', 'Mumbai '),
(75, 'Yogesh', 'Dwivedi', '9425469672', 'Service', '2021/03/26 15:52:45', '2021/03/26 17:05:52', '2021/03/25 21:23:14', 0, 'yogesh@mbcfpcl.org', 'Madhyabharat Vondortium.of FPCs Limited', 'Madhya Pradesh', 'Bhopal'),
(76, 'Mahesh', 'Rathore', '9799497116', 'Manufacturing ', NULL, NULL, '2021/03/25 21:46:43', 0, 'mahesh@maheshoil.com', 'Mahesh edible oil ind ltd', 'Rajasthan ', 'Kota'),
(77, 'Jignesh', 'Nayi', '9879546235', 'Broking', NULL, NULL, '2021/03/26 00:10:59', 0, 'nayi_jignesh@yahoo.com', 'Profit investment', 'gujarat', 'Unjha'),
(78, 'Sunandh', 'Subramaniam', '09987199398', 'Senior Research Analyst', NULL, NULL, '2021/03/26 04:38:09', 0, 'sunand.subramaniam@choiceindia.com', 'Mumbai', 'Maharashtra', 'India'),
(79, 'Nayan', 'Myatra', '9712208662', '', '2021/03/26 16:38:24', '2021/03/26 16:39:24', '2021/03/26 08:14:44', 0, 'atkissanpcl@gmail.com', 'Anjar', 'Gujarat', 'India'),
(80, 'Atul', 'Roongta', '9867555322', 'CFO', '2021/03/26 15:34:11', '2021/03/26 17:56:43', '2021/03/26 08:19:22', 0, 'atul.roongta@ncdex.com', 'NCDEX', 'Maharashtra', 'Mumbai'),
(81, 'Mangesh', 'Hulkey', '09890602080', 'NCCL', NULL, NULL, '2021/03/26 08:20:46', 0, 'mangesh.hulkey@nccl.co.in', 'Akola', 'Maharashtra', 'India'),
(82, 'Vinitha', 'Poojari', '9892374180', '', NULL, NULL, '2021/03/26 08:25:01', 0, 'vinitha.poojari@ncdex.com', 'NCDEX', 'maharashtra', 'mumbai'),
(83, 'Ashish', 'Bhasin', '9873010509', 'Senior manager', '2021/03/26 16:24:19', '2021/03/26 16:25:19', '2021/03/26 08:28:59', 0, 'ashish.bhasin@ncdex.com', 'Ncdex', 'Delhi', 'New Delhi'),
(84, 'Disha', 'Thakker', '9920751109', 'Service', NULL, NULL, '2021/03/26 08:40:40', 0, 'disha.thakker@nccl.co.in', 'National Commodity Clearing Ltd', 'Maharashtra', 'India'),
(85, 'Ashutosh', 'Deshpande', '9326716481', 'Market Linkages Head', '2021/03/26 15:24:07', '2021/03/26 17:15:49', '2021/03/26 08:40:42', 0, 'adeshpande107@gmail.com', 'Reliance Foundation', 'Maharashtra', 'Navi Mumbai'),
(86, 'Jayashree', 'B', '9840050444', '', '2021/03/26 16:17:19', '2021/03/26 17:05:41', '2021/03/26 08:55:59', 0, 'emailjayashree@gmail.com', '', 'Tamil Nadu', 'Chennai'),
(87, 'Rajeev', 'Tripathi', '09930339338', 'Vice President Legal', '2021/03/26 15:52:40', '2021/03/26 17:09:20', '2021/03/26 09:07:03', 0, 'rajeev.tripathi@ncdex.com', 'MUMBAI', 'Maharashtra', 'India'),
(88, 'j', 's', '9898882018', 'service', '2021/03/26 16:03:00', '2021/03/26 16:04:00', '2021/03/26 09:08:43', 0, 'jigar.shah@ncdex.com', 'ncdex', 'gujarat', 'ahmedabad'),
(89, 'Taslim', 'Arif', '8454043606', 'Service', NULL, NULL, '2021/03/26 09:09:41', 0, 'taslim.arif@reliancefoundation.org', 'Reliance Foundation', '1', '1'),
(90, 'MANOJ', 'SHIMPI', '9096442222', 'NCCL', '2021/03/26 16:57:07', '2021/03/26 16:58:07', '2021/03/26 09:12:12', 0, 'manojkumar.shimpi@nccl.co.in', 'NCCL', 'MAHARASHTRA', 'MUMBAI'),
(91, 'Mudit', 'Singhania ', '9820432223', 'Asset Management ', NULL, NULL, '2021/03/26 09:15:00', 0, 'mudit@alt-alpha.com', 'Alpha Alternatives', 'Maharashtra ', 'Mumbai'),
(92, 'Piyush', 'Jain', '9826732752', 'Job', '2021/03/26 15:30:55', '2021/03/26 15:31:55', '2021/03/26 09:18:08', 0, 'piyush.jain@ncdex.com', 'NCDEX', 'Madhya Pradesh', 'Indore'),
(93, 'Gaurav', 'Middha', '9108466245', '', NULL, NULL, '2021/03/26 09:22:43', 0, 'gaurav.middha@itc.in', 'ITC', 'Karnataka', 'Bangalore'),
(94, 'Mohit ', 'Bansal', '9911151820', 'Service', NULL, NULL, '2021/03/26 09:26:35', 0, 'mohitbansal@smcindiaonline.com', 'Delhi', 'DELHI', 'India'),
(95, 'Imran', 'Khan', '9765497868', 'Service', '2021/03/26 16:57:25', '2021/03/26 17:56:45', '2021/03/26 09:26:54', 0, 'imran.khan@ncdex.com', 'NCDEX', 'Maharashtra', 'Mumbai'),
(96, 'SACHIN', 'WAGLE', '09819887270', 'Service', '2021/03/26 16:56:21', '2021/03/26 17:56:58', '2021/03/26 09:34:17', 0, 'sachin.wagle@nerlindia.com', 'Mumbai', 'Maharashtra', 'India'),
(97, 'Shreya', 'Kothari', '9755319665', 'Employee', '2021/03/26 15:30:22', '2021/03/26 15:31:22', '2021/03/26 09:35:04', 0, 'shreya.kothari@ncdex.com', 'NCDEX', 'Maharashtra', 'Mumbai'),
(98, 'Anuradha', 'Singh', '9999005182', 'Working', '2021/03/26 15:40:25', '2021/03/26 17:04:42', '2021/03/26 09:41:26', 0, 'anuradha.singh@ncdex.com', 'NCDEX Limited', 'Delhi', 'New Delhi'),
(99, 'Sahil', 'Shah', '9833132128', 'Employee', '2021/03/26 15:33:25', '2021/03/26 15:34:25', '2021/03/26 09:49:52', 0, 'sahil.shah@ncdex.com', 'NCDEX', 'Maharashtra', 'Mumbai'),
(100, 'Mihir', 'Karandikar', '9920240370', 'Private Sector', '2021/03/26 15:45:00', '2021/03/26 15:46:00', '2021/03/26 09:50:34', 0, 'mihir.karandikar@ncdex.com', 'Mumbai', 'Maharashtra', 'India'),
(101, 'Uday Shankar', 'Jha', '9424282098', 'Service', '2021/03/26 19:24:25', '2021/03/26 19:25:25', '2021/03/26 09:53:01', 0, 'swfpcl.jeevika@gmail.com', 'Saharsa Women Jeevika Producer Company Ltd.', 'Bihar', 'Saharsa'),
(102, 'Mahesh', 'Desai', '9324118608', 'Service ', NULL, NULL, '2021/03/26 10:00:40', 0, 'maheshdesai@sunidhi.com', 'Sunidhi ', 'Maharashtra ', 'Mumbai '),
(103, 'satish', 'Sahane', '9967516380', 'Service', '2021/03/26 16:38:57', '2021/03/26 16:39:57', '2021/03/26 10:09:33', 0, 'satish.sahane@ncdex.com', 'NCDEX Ltd', 'Maharashtra', 'Mumbai'),
(104, 'Amit', 'Gupta', '9619551022', 'service', NULL, NULL, '2021/03/26 10:11:26', 0, 'amitg444@yahoo.com', '', 'maharashtra', 'mumbai'),
(105, 'Nisha ', 'Gupta', '8577031747', 'Anchor', '2021/03/26 15:32:52', '2021/03/26 15:33:52', '2021/03/26 10:14:37', 0, 'nishii1298@gmail.com', 'Market Times TV', 'Uttar Pradesh ', 'Noida '),
(106, 'Bhupalini', 'Kodati', '9885633926', 'Product Manager', '2021/03/26 15:33:36', '2021/03/26 15:34:36', '2021/03/26 10:16:40', 0, 'bhupalini.kodati@ncdex.com', 'NCDEX', 'Maharashtra', 'Mumbai'),
(107, 'Bipin', 'Puthran', '9820788169', '', '2021/03/26 15:35:27', '2021/03/26 15:36:27', '2021/03/26 10:25:49', 0, 'bipin.puthran@ncdex.com', 'NCDEX', 'Maharashtra', 'Mumbai'),
(108, 'Ankur', 'kothari', '9893090900', 'compliance officer', '2021/03/26 16:35:13', '2021/03/26 16:44:03', '2021/03/26 10:27:12', 0, 'futures@elkayonline.com', 'lakhanlal kantilal brokers pvt ltd', 'madhya pradesh', 'indore'),
(109, 'rajkumar', 'patel', '9617420600', 'trader', '2021/03/26 16:00:23', '2021/03/26 16:01:23', '2021/03/26 10:29:10', 0, 'rajkumar.patel@vippysoya.com', 'vippy industries ltd.', 'madhya pradesh', 'indore'),
(110, 'Vijay', 'Udas', '9309945321', '', NULL, NULL, '2021/03/26 10:31:50', 0, 'vijayudas54@gmail.com', 'Vitthal trading company', 'Maharashtra', 'Hingoli'),
(111, 'Pramod', 'Tonge', '9422616250', 'Director', NULL, NULL, '2021/03/26 10:43:07', 0, 'agrosamriddhi@gmail.com', 'Saivishwa Farmer Producer Company Ltd', 'Maharashtra', 'Osmanabad'),
(112, 'Shrikrushna ', 'Shinde', '7722025803', 'Director', '2021/03/26 15:43:05', '2021/03/26 16:14:50', '2021/03/26 10:45:11', 0, 'chandolfpcompany@gmail.com', 'Chandol Farmers Producer Company Limited', 'Maharashtra', 'Buldhana'),
(113, 'Virendra singh', 'Songra', '7389738937', 'Farmer', NULL, NULL, '2021/03/26 10:45:29', 0, 'samradhakisanfpoujjain@gmail.com', 'Samradha kpcl', 'Mp', 'Ujjain'),
(114, 'Jitendra Singh', 'Rajput', '6260804174', '', NULL, NULL, '2021/03/26 10:45:52', 0, 'fpcmandsaur1@gmail.com', 'Mandsaur farmer producer company LTD', 'Madhya Pradesh', 'Mandsaur'),
(115, 'Abhiman ', 'Auchar ', '9421945001', 'Preshident ', NULL, NULL, '2021/03/26 11:01:05', 0, 'balirajakvm@gmail.com', 'Creative shetkari producer company Ltd borkhed Tq dist beed ', 'Maharashtra ', 'Beed'),
(116, 'Ashok ', 'Landge', '9405749500', 'Business ', '2021/03/26 16:44:34', '2021/03/26 16:45:34', '2021/03/26 11:03:56', 0, 'bapcltd48@gmail.com', 'Bhavarthdipika Agro Producer Company Ltd', 'Maharashtra ', 'Kalamb'),
(117, 'Panjabrao', 'Choure', '9545086604', 'Farmer', NULL, NULL, '2021/03/26 11:10:59', 0, 'shashwatiagroproducer@gmail.com', 'Shashwati agro producer company', 'Maharashtra', 'Beed'),
(118, 'Hemant', 'Singhvi', '9820114219', '', '2021/03/26 18:47:32', '2021/03/26 18:48:32', '2021/03/26 11:17:02', 0, 'hemant.singhvi@nccl.co.in', '', 'Maharashtra ', 'Mumbai'),
(119, 'Mukesh', 'Kushwaha', '8058604542', '', NULL, NULL, '2021/03/26 11:19:36', 0, 'info@navjeevanbroking.com', 'NAVJEEVAN TRADE AND COMMODITIES PVT LTD', 'Rajasthan', 'Jaipur'),
(120, 'Bhanwar', 'Nitharwal', '9829890678', '', NULL, NULL, '2021/03/26 11:26:20', 0, 'rms@navjeevanbroking.com', 'Navjeevan Trade & commodities Pvt Ltd', 'Rajasthan', 'Jaipur'),
(121, 'Ashish', 'Sarnaik', '7020087875', 'Job', NULL, NULL, '2021/03/26 11:52:52', 0, 'ashishsarnaik34@gmail.com', 'NCCL', 'MH', 'Akola'),
(122, 'Rohit', 'Suvarna', '9819434918', 'IT', '2021/03/26 15:30:55', '2021/03/26 15:53:12', '2021/03/26 12:12:19', 0, 'rohit.suvarna@ncdex.com', 'NCDEX', 'Maharashtra', 'Mumbai'),
(123, 'Abhijeet', 'Nikam', '9819152804', 'Service', '2021/03/26 16:11:50', '2021/03/26 16:12:50', '2021/03/26 12:13:49', 0, 'abhijeet.nikam@ncdex.com', 'NCDEX', 'Maharashtra', 'Mumbai'),
(124, 'KANA', 'MEENA', '9829833525', '', '2021/03/26 15:52:34', '2021/03/26 16:02:42', '2021/03/26 12:15:56', 0, 'kanaram.meena@ncdex.com', 'NCDEX', 'RAJASTHAN', 'Jaipur'),
(125, 'Shreekanth ', 'Shivram', '9823398556', 'Service', '2021/03/26 16:02:46', '2021/03/26 16:14:47', '2021/03/26 12:20:51', 0, 'shreekanth.shivram@ncdex.com', 'NCDEX', 'Maharashtra', 'Mumbai'),
(126, 'Umang', 'Kapoor', '8080254106', 'Salaried', '2021/03/26 17:02:59', '2021/03/26 17:03:59', '2021/03/26 12:27:01', 0, 'umang.kapoor@nerlindia.com', 'NERL', 'Maharashtra', 'Mumbai'),
(127, 'Sukant', 'Arora', '9999991334', 'Business', '2021/03/26 21:06:02', '2021/03/26 21:07:02', '2021/03/26 12:33:51', 0, 'sukant.arora@shareindia.com', 'Share India Securities Ltd', 'Delhi', 'New Delhi'),
(128, 'Shivam', 'Saraswat', '9759022477', 'Arbitrageur', NULL, NULL, '2021/03/26 12:38:32', 0, 'shivam878996@gmail.com', 'R money', 'Uttar Pradesh', 'Agra'),
(129, 'Rajkumar', 'Patel', '7000102359', '', '2021/03/26 15:54:56', '2021/03/26 17:44:56', '2021/03/26 12:39:18', 0, 'farmerfuture.fpo@gmail.com', 'Farmer Future Producer Company Ltd', 'MP', 'Khandwa'),
(130, 'BHAVYA', 'VIJAY', '09899805806', '', '2021/03/26 15:52:10', '2021/03/26 15:53:10', '2021/03/26 12:40:26', 0, 'bhavya.v@algowire.com', 'Delhi', 'Delhi', 'India'),
(131, 'Neha', 'Singh', '9867696009', '', '2021/03/26 15:54:30', '2021/03/26 15:55:30', '2021/03/26 12:41:56', 0, 'neha.singh@ncdex.com', 'NCDEX ', 'Maharashtra', 'Mumbai'),
(132, 'Atul', 'GODSE', '8087122013', 'Director', NULL, NULL, '2021/03/26 12:49:47', 0, 'sewarthafpo@gmail.com', 'Sewartha Agro Producer company limited murtizapur', 'Maharashtra', 'Murtizapur'),
(133, 'Kapil', 'Mittal', '09897300999', '', '2021/03/26 17:09:44', '2021/03/26 17:10:44', '2021/03/26 12:50:09', 0, 'kapilmittal@rmoneyindia.com', 'Agra', 'UP', 'India'),
(134, 'Manish ', 'Malhotra', '9815991291', 'VP', '2021/03/26 15:24:43', '2021/03/26 15:25:43', '2021/03/26 13:00:03', 0, 'manish.malhotra@myfindoc.com', 'FINDOC Financial Services Group ', 'Delhi', 'Delhi'),
(135, 'Dalip ', 'Sharma ', '9711946403', '', '2021/03/26 15:40:15', '2021/03/26 15:41:15', '2021/03/26 13:00:55', 0, 'dilipsharma036@gmail.com', 'Share india securities ltd', 'Delhi ', 'Delhi '),
(136, 'POOJA', 'PANWAR', '9352001263', 'ACCOSIATE', '2021/03/26 16:36:03', '2021/03/26 16:54:58', '2021/03/26 13:01:35', 0, 'panwarpooja263@gmail.com', 'SHREE BALAJI MULTICOMMODITIES PVT.LTD', 'RAJASTHAN', 'SRI GANGANAGAR'),
(137, 'AMIT ', 'SHARMA', '9461183838', 'ASSOCIATE', '2021/03/26 15:41:43', '2021/03/26 15:42:43', '2021/03/26 13:03:33', 0, 'amit83838@gmail.com', 'SHREE BALAJI MULTICOMMODITIES PVT.LTD', 'RAJASTHAN', 'SRI GANGANAGAR'),
(138, 'Arun', 'Yadav', '8156006791', '', '2021/03/26 15:25:16', '2021/03/26 15:26:16', '2021/03/26 13:04:52', 0, 'arun.yadav@ncdex.com', 'NCDEX', 'Rajasthan', 'Jaipur'),
(139, 'SAHIL', 'GUPTA', '9414087886', 'ASSOCIATE', '2021/03/26 15:43:44', '2021/03/26 15:45:32', '2021/03/26 13:04:57', 0, 'sahilgbittu1991@gmail.com', 'SHREE BALAJI MULTICOMMODITIES PVT.LTD', 'RAJASTHAN', 'SRI GANGANAGAR'),
(140, 'YUVRAJ', 'KANOONGO', '9001747474', 'Durector', '2021/03/26 15:24:51', '2021/03/26 15:25:51', '2021/03/26 13:07:51', 0, 'yuvraj@htplonline.com', 'Hindustan Tradecom Private Limited', 'Rajasthan', 'Jaipur'),
(141, 'PUNEET', 'ARORA', '9982881088', 'ASSOCIATE', '2021/03/26 17:01:12', '2021/03/26 17:02:12', '2021/03/26 13:08:05', 0, 'puneetarora2626@gmail.com', 'SHREE BALAJI MULTICOMMODITIES PVT.LTD', 'RAJASTHAN', 'SRI GANGANAGAR'),
(142, 'Sachin', 'Agarwal', '9319200246', 'Head pms', '2021/03/26 15:44:58', '2021/03/26 17:46:08', '2021/03/26 13:08:06', 0, 'sachin.agarwal@rmoneyindia.com', 'Raghunandan capital pvt ltd', 'Up', 'Agra'),
(143, 'sachin', 'gupta', '7987242450', 'Professional', '2021/03/26 17:12:13', '2021/03/26 17:13:13', '2021/03/26 13:09:05', 0, 'sachin.gupta@choiceindia.com', 'Choice Equity Broking', 'Maharashtra ', 'Mumbai'),
(144, 'Durgesh', 'Kumar', '6396665722', 'Arbitrageur', NULL, NULL, '2021/03/26 13:09:10', 0, 'durgeshkumaragra88@gmail.com', 'Raghunandanmoney', 'Uttar pradesh', 'Agra'),
(145, 'Sarvjeet singh', 'Batra', '8800971245', 'Arbitrager', NULL, NULL, '2021/03/26 13:09:11', 0, 'sarvjeetbatra@yahoo.co.uk', 'Raghunandan capital', 'Delhi', 'New Delhi'),
(146, 'PANKAJ ', 'KEDIA', '9887272615', 'ASSOCIATE', '2021/03/26 15:35:24', '2021/03/26 16:59:36', '2021/03/26 13:11:11', 0, 'kediapankaj615@gmail.com', 'SHREE BALAJI MULTICOMMODITIES PVT.LTD', 'RAJASTHAN', 'SRI GANGANAGAR'),
(147, 'Raj', 'K', '7678672223', 'Traddr', '2021/03/26 16:22:22', '2021/03/26 18:22:40', '2021/03/26 13:11:12', 0, 'bhardwaj.rock1985@gmail.com', 'R money', 'Delhi', 'Delhi'),
(148, 'Harshad ', 'Thakker', '9427375221', 'Bizens', NULL, NULL, '2021/03/26 13:11:41', 0, 'keshavharij@gmail.com', 'Keshav trading co ', 'Gujaret', 'Unjha'),
(149, 'Anish', 'KANOONGO', '9829790071', 'Manager', NULL, NULL, '2021/03/26 13:11:41', 0, 'anish@htplonline.com', 'Hindustan technosol pvt ltd', 'Rajasthan', 'Jaipur'),
(150, 'Abadh pal', 'Sing', '9891173579', '', NULL, NULL, '2021/03/26 13:13:22', 0, 'abadhpal310@gmail.com', 'Share india', 'Delhi', 'Delhi'),
(151, 'Vikas ', 'Sharma ', '9416691528', 'Dealer', '2021/03/26 19:19:48', '2021/03/26 19:20:48', '2021/03/26 13:13:51', 0, 'vikasshrm143@gmail.com', 'RAGHUNANDAN capital private limited ', 'Delhi', 'Delhi'),
(152, 'Sachin ', 'Bhokare ', '9099037006', '', '2021/03/26 15:31:20', '2021/03/26 15:32:20', '2021/03/26 13:13:56', 0, 'sachin.bhokare@ncdex.com', 'NCDEX ', 'Gujarat ', 'Ahmedabad '),
(153, 'Sharath', 'Thodupunoori', '7842443336', 'Employee', '2021/03/26 16:57:14', '2021/03/26 16:58:14', '2021/03/26 13:14:13', 0, 'sharatht133@gmail.com', 'Ncdex', 'Gujarat', 'Ahmedabad'),
(154, 'BHARAT', 'SONI', '9602696016', 'ACCOSIATE', '2021/03/26 15:37:23', '2021/03/26 15:38:23', '2021/03/26 13:15:09', 0, 'RAVISONI180@GMAIL.COM', 'SHREE BALAJI MULTICOMMODITIES PVT.LTD', 'RAJASTHAN', 'SRI GANGANAGAR'),
(155, 'Shubham', 'Jain', '0', 'NCDEX', NULL, NULL, '2021/03/26 13:15:26', 0, 'economistshubhamjain@gmail.com', 'AGRA', 'UTTAR PRADESH', 'India'),
(156, 'Atul', 'Midha', '7240172402', '', NULL, NULL, '2021/03/26 13:17:49', 0, 'atulmidha@gclbroking.com', 'Ganganagar Commodity Ltd', 'Rajasthan', 'SriGanganagar'),
(157, 'Amit', 'Malde', '9819136563', '', '2021/03/26 15:35:15', '2021/03/26 15:36:15', '2021/03/26 13:17:57', 0, 'amit.malde@ncdex.com', 'NCDEX', 'Maharashtra', 'Mumbai'),
(158, 'DEEPSHIKHA', 'GILL', '9849964522', 'ASSOCIATE', NULL, NULL, '2021/03/26 13:18:02', 0, 'shikhadeep1082@gmail.com', 'SHREE BALAJI MULTICOMMODITIES PVT.LTD', 'RAJSTHAN', 'SRI GANGANAGAR'),
(159, 'Anupam', 'Chaturvedi', '8003293195', 'Service', '2021/03/26 15:27:34', '2021/03/26 15:28:34', '2021/03/26 13:18:15', 0, 'anupam.chaturvedi@ncdex.com', 'Ncdex', 'Gujarat', 'Ahmedabad'),
(160, 'Sujit ', 'Kumar ', '8447247650', 'Arbirager ', NULL, NULL, '2021/03/26 13:19:39', 0, 'sujit15sep1982@gmail.com', 'Share india', 'Delhi', 'Delhi '),
(161, 'Pramod', 'Singh', '7743001859', '', '2021/03/26 16:02:27', '2021/03/26 16:54:28', '2021/03/26 13:21:07', 0, 'pramod.singh@myfindoc.com', 'Findooc', 'Up', 'Ghaziabad'),
(162, 'Ravi', 'Singhal', '9873074981', 'Business', '2021/03/26 15:22:48', '2021/03/26 15:23:48', '2021/03/26 13:22:08', 0, 'ravisinghal@gclbroking.com', 'Ganganagar Commodity Ltd', 'Rajasthan', 'Jaipur'),
(163, 'Devesh', 'Bhatt', '9820810500', '', NULL, NULL, '2021/03/26 13:22:41', 0, 'devesh.bhatt@iifl.com', 'IIFL Securities ', 'Maharashtra', 'Mumbai'),
(164, 'Ashish ', 'Naphade', '9545755200', 'Professional', '2021/03/26 16:53:52', '2021/03/26 17:56:16', '2021/03/26 13:23:36', 0, 'jaysardarfpc@gmail.com', 'Jay sardar fpc', 'Maharashtra', 'Malkapur'),
(165, 'DEEPAK ', 'JAIN', '9174673099', 'SALES MANAGER', '2021/03/26 15:29:12', '2021/03/26 15:30:12', '2021/03/26 13:25:36', 0, 'DEEPAK.JAIN@PRITHVIFINMART.COM', 'PRITHVI FINMART PVT LTD', 'MADHYA PRADESH', 'INDORE'),
(166, 'Priyam ', 'Bansal ', '9811343471', '', NULL, NULL, '2021/03/26 13:28:31', 0, 'Priyambansal@globecapital.com', 'Globe Commodities Ltd ', 'Delhi', 'Delhi'),
(167, 'Jogender', 'Singh', '9991992479', 'Job', '2021/03/26 15:34:40', '2021/03/26 15:35:40', '2021/03/26 13:29:59', 0, 'jogender.singh@myfindoc.com', 'Findoc Financial Service', 'Haryana', 'Gurgaon'),
(168, 'Sanjay', 'Chauhan ', '9915609244', 'Service ', '2021/03/26 16:26:47', '2021/03/26 16:27:47', '2021/03/26 13:30:02', 0, 'sanjay008chauhan@gmail.com', 'Findoc', 'Hariyana ', 'Panchkula '),
(169, 'ANKIT', 'CHANGIA', '8696100003', 'CA', NULL, NULL, '2021/03/26 13:30:04', 0, 'ankitchangia@gmail.com', 'SHREE BALAJI MULTICOMMODITIES PVT.LTD', 'RAJASTHAN', 'SRI GANGANAGAR'),
(170, 'Hemant', 'Kalra', '9915700677', 'Service', '2021/03/26 15:25:02', '2021/03/26 15:26:02', '2021/03/26 13:30:12', 0, 'hemantkalra42@gmail.com', 'Findoc investmart ', 'Ut', 'Chandigarh '),
(171, 'Lakshmi', 'Upreti', '9899237857', 'Manager', '2021/03/26 15:35:01', '2021/03/26 15:36:01', '2021/03/26 13:31:21', 0, 'lakshmiupreti91@gmail.com', 'findoc', 'delhi', 'delhi'),
(172, 'manish', 'kumar', '8837521403', 'job', '2021/03/26 15:29:54', '2021/03/26 15:30:54', '2021/03/26 13:32:19', 0, 'PCGDESK@MYFINDOC.COM', 'findoc', 'delhi', 'new delhi'),
(173, 'Vipin ', 'Chopra', '9136419658', 'Trader', NULL, NULL, '2021/03/26 13:34:49', 0, 'mrchopra49@gmail.com', 'Ragunandan money', 'Delhi', 'Delhi'),
(174, 'Shabnam', 'Bhagat', '7508449552', 'Pvt job', NULL, NULL, '2021/03/26 13:34:53', 0, 'shabnambhagat90@gmail.com', 'Findoc financial services group', 'Punjab', 'Ludhiana'),
(175, 'Abhishek ', 'Chauhan', '9930997186', 'Service', NULL, NULL, '2021/03/26 13:41:23', 0, 'abhishch@gmail.com', 'SWASTIKA ', 'Madhya pradesh', 'INDORE'),
(176, 'Sunny', 'Bhatia', '9855239100', 'Service ', NULL, NULL, '2021/03/26 13:46:41', 0, 'sunny.mcsl@gmail.com', 'Findoc', 'Punjab ', 'Ludhiana '),
(177, 'Rakesh', 'Kaushal', '9803046000', 'Service', '2021/03/26 15:31:50', '2021/03/26 15:32:50', '2021/03/26 13:47:35', 0, 'rakesh.k@myfindoc.com', 'Findoc', 'UT', 'Chandigarh'),
(178, 'Sonia', 'Mittal', '9910533459', 'Job', '2021/03/26 17:46:01', '2021/03/26 17:47:01', '2021/03/26 13:47:45', 0, 'sonia.mittal@yahoo.co.in', 'Raghunandan money', 'Delhi', 'Delhi'),
(179, 'Suraj', 'Prasad', '9803037000', '', NULL, NULL, '2021/03/26 13:48:30', 0, 'surajasp@gmail.com', 'Findoc', 'Punjab', 'Ludhiana'),
(180, 'Ratika', 'Khanna', '9888909518', 'Service Findoc', NULL, NULL, '2021/03/26 13:49:18', 0, 'kyc@myfindoc.com', 'Ludhiana', 'Punjab', 'India'),
(181, 'Meenakshi', 'Sharma', '8437183935', 'Service', '2021/03/26 15:35:18', '2021/03/26 15:36:18', '2021/03/26 13:50:12', 0, 'meenakshi.binjola95@gmail.com', 'Findoc investmart private limited', 'Punjab', 'Ludhiana'),
(182, 'Azad', 'KUMAR', '8727048600', 'Job', '2021/03/26 16:56:17', '2021/03/26 16:57:17', '2021/03/26 13:51:36', 0, 'azadkpankaj@yahoo.co.in', 'CHANDIGARH', 'CHANDIGARH', 'Chabdigarh'),
(183, 'Radha', 'Gaba ', '9004188800', 'Broker', NULL, NULL, '2021/03/26 13:56:10', 0, 'radhagaba@gmail.com', 'Findoc', 'Maharashtra ', 'Mumbai '),
(184, 'Rajib', 'Saha', '9949994144', 'Service', '2021/03/26 15:33:31', '2021/03/26 17:56:43', '2021/03/26 14:00:27', 0, 'Rajib.Saha@itc.in', 'ITC LIMITED', 'AP', 'GUNTUR'),
(185, 'Mohan ', 'Bhise', '09404551000', 'Aroma Organice Agro Producer Company Limited', NULL, NULL, '2021/03/26 14:01:21', 0, 'aroma.organicfpc@gmail.com', 'Latur', 'Maharashtra', 'India'),
(186, 'Anshul', 'Sadani', '09320608186', 'Service ', NULL, NULL, '2021/03/26 14:02:39', 0, 'anshul.sadani@proventusagro.com', 'Mumbai', 'Maharashtra', 'India'),
(187, 'Naval', 'Sharma', '9888901180', 'Private job', NULL, NULL, '2021/03/26 14:04:32', 0, 'sharma.naval.1983@gmail.com', 'Kotak', 'Ut', 'Chandigarh'),
(188, 'Vishal', 'Kesarwani', '9026793462', '', '2021/03/26 15:39:06', '2021/03/26 15:40:06', '2021/03/26 14:06:20', 0, 'vishal.kesarwani17@gmail.com', 'Religare Broking ltd', 'Uttar Pradesh', 'India'),
(189, 'Ashok', 'Vashisth', '7898995590', 'Agribusiness ', NULL, NULL, '2021/03/26 14:10:00', 0, 'ashokvashist1503@gmail.com', 'Sheopur agro producer company Ltd ', 'Madhyapradesh ', 'Sheopur'),
(190, 'Nitin', 'Shahi', '9781848000', 'Service', '2021/03/26 15:49:48', '2021/03/26 15:50:48', '2021/03/26 14:14:20', 0, 'ns@myfindoc.com', 'FINDOC ', 'Punjab ', 'Ludhiana '),
(191, 'Inder', 'Arya', '9560735347', 'Service ', '2021/03/26 15:28:14', '2021/03/26 15:29:14', '2021/03/26 14:16:38', 0, 'inder.arya@myfindoc.com', 'Findoc Investmart Pvt.Ltd ', 'Uttar Pradesh', 'Ghaziabad '),
(192, 'Mukesh', 'Goyal', '9810539702', 'Service', '2021/03/26 15:25:19', '2021/03/26 17:29:34', '2021/03/26 14:17:05', 0, 'mukeshgoyal@globecapital.com', 'Globe Commodities Ltd.', 'Delhi', 'New Delhi'),
(193, 'Chetan', 'Kasliwal', '9404051008', 'Service', '2021/03/26 15:48:34', '2021/03/26 15:49:34', '2021/03/26 14:17:29', 0, 'chetan.kasliwal@religare.com', 'Religare Broking Ltd', 'Maharashtra', 'Jalgaon '),
(194, 'Nitin', 'wandile', '7720938337', 'Farming', NULL, NULL, '2021/03/26 14:18:06', 0, 'nitinwandile1988@gmail.com', 'wanashish m fpc', 'maharashtra', 'wardha'),
(195, 'Neelesh', 'Singh', '9589851702', 'CEO', '2021/03/26 17:24:16', '2021/03/26 17:25:16', '2021/03/26 14:18:29', 0, 'spclsidhi@gmail.com', 'Sontat Producer Company limited', 'Madhya Pradesh', 'Sidhi'),
(196, 'HUNNY', 'GARG', '9887716680', 'ASSOCIATE', '2021/03/26 16:37:38', '2021/03/26 16:38:38', '2021/03/26 14:18:35', 0, 'hynnygarg@sbmcpl.co.in', 'SHREE BALAJI MULTICOMMODITIES PVT.LTD', 'RAJASTHAN', 'SRI GANGANAGAR'),
(197, 'Satyen', 'Rairana ', '9007010007', 'Capital Markets ', NULL, NULL, '2021/03/26 14:21:51', 0, 'satyen@radarvision.com', 'Radar Vision Ltd ', 'West Bengal ', 'Kolkata '),
(198, 'Ajinder', 'Singh', '7009765825', 'Accountant', '2021/03/26 17:05:02', '2021/03/26 17:06:02', '2021/03/26 14:21:53', 0, 'ajinder@myfindoc.com', 'Findoc Financial Services Group', 'Punjab', 'Ludhiana'),
(199, 'Atul', 'mongia', '09803051000', '', NULL, NULL, '2021/03/26 14:23:07', 0, 'atulmyfindoc@gmail.com', 'Ludhianaa', 'Punjab', 'India'),
(200, 'SUMANTAN ', 'GHOSH', '9903944617', 'service', '2021/03/26 15:48:49', '2021/03/26 15:57:18', '2021/03/26 14:24:29', 0, 'sumantan.ghosh@religare.com', 'Religare Broking Ltd', 'WestBengal', 'kolkata'),
(201, 'Venkatagiri', 'Shetty', '9611401402', 'Employee', '2021/03/26 15:45:06', '2021/03/26 17:26:22', '2021/03/26 14:25:11', 0, 'venkatagiri.b@itc.in', 'ITC Limited', 'AP', 'Guntur'),
(202, 'Vidwan', 'KANOONGO', '9928312000', 'Compliance Officer', NULL, NULL, '2021/03/26 14:27:05', 0, 'vidwan@htplonline.com', 'Hindustan Technosol Private Limited', 'Rajasthan', 'Jaipur'),
(203, 'Vilas', 'Uphade', '09823733322', '', NULL, NULL, '2021/03/26 14:31:19', 0, 'vikasagro.fpc@gmail.com', 'Latur', 'Maharashtra', 'India'),
(204, 'Tanushree', 'Dave', '19930168521', 'Professional', NULL, NULL, '2021/03/26 14:31:21', 0, 'dave.tanu@gmail.com', 'Ahmedabad', 'Gujarat', 'India'),
(205, 'Jitendra ', 'Raisinghania ', '9810990862', 'Salaried', NULL, NULL, '2021/03/26 14:32:45', 0, 'jitendra.raisinghania@gmail.com', 'Findoc', 'Delhi', 'Delhi'),
(206, 'Neeraj ', 'Tiwari ', '9826924442', '', '2021/03/26 16:41:58', '2021/03/26 16:42:58', '2021/03/26 14:32:55', 0, 'neeraj.tiwari@nerlindia.com', 'NERL', 'Gujarat', 'Ahmedabad '),
(207, 'jigar', 'Mehta', '09428222455', '', '2021/03/26 15:40:35', '2021/03/26 15:41:35', '2021/03/26 14:33:05', 0, 'jigar.mehta@neml.in', 'AHMEDABAD', 'gujarat', 'India'),
(208, 'Vibhore', 'Gupta', '9289781026', 'Service', '2021/03/26 16:59:38', '2021/03/26 17:00:38', '2021/03/26 14:34:43', 0, 'vibhore@myfindoc.com', 'Findoc', 'Delhi', 'Delhi'),
(209, 'Santosh', 'Rout', '9643304935', 'Service', '2021/03/26 15:58:10', '2021/03/26 15:59:10', '2021/03/26 14:41:33', 0, 'Santosh.rout@iifl.com', 'IIFL', 'Delhi', 'New delhi'),
(210, 'MANOHAR ', 'Singh', '07024667773', 'Service ', NULL, NULL, '2021/03/26 14:43:21', 0, 'manojanjana79@gmail.com', 'Mandsor', 'Mp', 'India'),
(211, 'Sharad', 'Jain', '9313678891', 'Service', '2021/03/26 15:46:48', '2021/03/26 15:47:48', '2021/03/26 14:43:56', 0, 'Sharadjain@globecapital.com', 'Globe Capital Market Ltd.', 'Delhi', 'NEW DELHI'),
(212, 'Karan', 'Sachdeva', '9990783672', 'Self trader', NULL, NULL, '2021/03/26 14:45:24', 0, 'ksachdeva0307@gmail.com', 'Raghunandan money', 'Delhi', 'Delhi'),
(213, 'Naveen ', 'Mathur', '9920139299', 'Service ', NULL, NULL, '2021/03/26 14:45:33', 0, 'naveen.mathur@rathoi.com', 'Anand Rathi ', 'Maharashtra ', 'Mumbai'),
(214, 'Sandeep', 'Agarwal', '9821130987', 'Trader', '2021/03/26 16:19:45', '2021/03/26 16:20:45', '2021/03/26 14:45:47', 0, 'sandeepharikisanka@gmail.com', 'Gautam Budh Nagar', 'Uttar Pradesh', 'India'),
(215, 'Gaurav', 'Sharma', '9068339122', 'Trader', NULL, NULL, '2021/03/26 14:46:17', 0, 'sharmagaurav16117@gmail.com', 'Share india', 'Haryana', 'Sonepat'),
(216, 'Amit', 'Sharma', '9811543898', 'Business ', NULL, NULL, '2021/03/26 14:46:25', 0, 'amit20_sharma@yahoo.co.in', '', 'Delhi', 'Delhi'),
(217, 'Chetan', 'Bharkhada', '09833913703', 'Service', NULL, NULL, '2021/03/26 14:46:54', 0, 'chetanbharkhada@rathi.com', 'Mumbai', 'Maharastra', 'India'),
(218, 'Rohit ', 'Jain', '9891607762', 'Saleried', NULL, NULL, '2021/03/26 14:48:06', 0, 'rohitjain1432@gmail.com', 'Raghunandan money', 'Haruana', 'Sirsa'),
(219, 'Rahul ', 'Ganatra', '9820817350', '', '2021/03/26 15:25:42', '2021/03/26 15:26:42', '2021/03/26 14:48:26', 0, 'rahul.ganatra@nccl.co.in', 'NCCL ', 'Maharashtra ', 'Mumbai '),
(220, 'DK ', 'AGGARWAL', '9810032371', 'BUSINESSMAN', '2021/03/26 15:31:53', '2021/03/26 15:32:53', '2021/03/26 14:48:42', 0, 'dka@smcindiaonline.com', 'delhi', 'DELHI', 'DELHI'),
(221, 'Banwari ', 'Lal', '8010758015', 'Trader', NULL, NULL, '2021/03/26 14:49:02', 0, 'banwari011986@gmail.com', 'Share india', 'Delhi', 'New Delhi '),
(222, 'Ankiy', 'Changia', '7727800003', '', NULL, NULL, '2021/03/26 14:49:32', 0, 'Achangia@sbmcpl.co.in', 'SBJ Multicom PL', 'Raj', 'Sri Ganganagar'),
(223, 'Gaurav', 'mittal', '9716494045', 'trader', NULL, NULL, '2021/03/26 14:49:40', 0, 'gauravmittal97@gmail.com', 'shareindia', 'delhi', 'delhi'),
(224, 'Chetan', 'Bharkhada', '9833913703', 'Service', NULL, NULL, '2021/03/26 14:49:54', 0, 'chetanbharkhada@rathi.com', 'Mumbai', 'Maharastra', 'India'),
(225, 'Neerav', 'Parmar', '9638332696', 'Service', '2021/03/26 15:26:06', '2021/03/26 15:27:06', '2021/03/26 14:52:42', 0, 'tpp@myfindoc.com', 'Findoc Capital Mart Pvt Ltd', 'Gujarat', 'Ahmedabad'),
(226, 'Umang ', 'Agrawal', '9829324322', 'Commodity head - Geojit', NULL, NULL, '2021/03/26 14:52:46', 0, 'umang_ra@geojit.com', 'Jaipur', 'RAJASTHAN', 'India'),
(227, 'Pankaj ', 'Khare', '9319011556', 'Service', NULL, NULL, '2021/03/26 14:54:30', 0, 'pankaj_khare@geojit.com', 'Geojit financial services lrd', 'Uttar Pradesh ', 'Lucknow'),
(228, 'Ishan', 'Sharma', '9803252000', 'Stock market ', '2021/03/26 17:01:27', '2021/03/26 17:02:27', '2021/03/26 14:55:18', 0, 'ishan.sharma@myfindoc.com', 'Findoc', 'Punjab', 'Ludhiana '),
(229, 'Nikhil', 'Mehra', '8700602137', 'Trader', NULL, NULL, '2021/03/26 14:56:53', 0, 'Nikhil.mhr100@gmail.com', 'Share India', 'Delhi', 'New Delhi'),
(230, 'Santosh Raj', 'Koppula', '9100597677', 'Employee', NULL, NULL, '2021/03/26 14:58:19', 0, 'ksantoshraj493@gmail.com', 'Geojit financial services Ltd', 'Telangana', 'Hyderabad'),
(231, 'Jayvant ', 'Thakkar', '9879890910', 'Services', '2021/03/26 16:56:13', '2021/03/26 16:57:13', '2021/03/26 14:59:21', 0, 'jayvantthakkar@gmail.com', 'MSFL', 'Gujarat', 'Ahmedabad'),
(232, 'Nitesh', 'Garg', '8288020794', 'Service', '2021/03/26 15:28:38', '2021/03/26 15:29:38', '2021/03/26 15:02:05', 0, 'nitesh@myfindoc.com', 'Findoc', 'Punjab', 'Ludhiana'),
(233, 'Akhilesh', 'Mishra', '9560360091', 'Self emply', NULL, NULL, '2021/03/26 15:02:48', 0, 'akhileshkmishra1968@gmail.com', 'SHARE ONDIA SECURITIES', 'Delhi', 'Delhi'),
(234, 'Manohar Singh', 'Tomar', '9893266930', 'Job', '2021/03/27 06:45:03', '2021/03/27 06:45:28', '2021/03/26 15:06:01', 0, 'tomarmanohar@yahoo.com', 'Kalisindh farmar producer dhabla hardu', 'MP', 'Ujjain'),
(235, 'Rachit', 'Gupta', '8375830665', 'Business ', '2021/03/26 17:17:27', '2021/03/26 17:18:27', '2021/03/26 15:07:17', 0, 'rachit@shareindia.com', 'Shareindia securities ltd', 'Delhi', 'Delhi'),
(236, 'Shubham', 'Naphade', '7887756459', 'Job', NULL, NULL, '2021/03/26 15:07:17', 0, 'shubhamnaph113@gmail.com', 'Jay sardar Agri company', 'Maharashtra', 'Malkapur'),
(237, 'PUNIT', 'CHHABILE', '9503068024', 'JOB', '2021/03/26 15:17:38', '2021/03/26 15:18:38', '2021/03/26 15:08:09', 0, 'PUNIT.CHHABILE@GMAIL.COM', 'NCDEX', 'Madhya Pradesh', 'INDORE'),
(238, 'Sourav', 'Jain', '8078653605', 'Broking', '2021/03/26 16:18:45', '2021/03/26 16:19:45', '2021/03/26 15:09:43', 0, 'Sorabh94145@gmail.com', 'bikaner', 'RAJASTHAN', 'India'),
(239, 'joysen', 'paul', '9930012323', '', '2021/03/26 16:23:29', '2021/03/26 16:24:29', '2021/03/26 15:09:55', 0, 'joysenp@gmail.com', 'NCDEX', 'Maharashtra', 'MUMBAI'),
(240, 'Mukesh', 'Natani', '9314501569', 'Business', NULL, NULL, '2021/03/26 15:10:02', 0, 'mukeshnatani@gmail.com', 'Balaji trading corporation', 'Rajasthan', 'Jaipur'),
(241, 'Alok ', 'Verma ', '9205193674', '', NULL, NULL, '2021/03/26 15:10:11', 0, 'alok.verma@adm.com', 'ADM ', 'Haryana ', 'Gurgaon '),
(242, 'Deepak kumar ', 'Sharma', '9999823003', 'Trading commodity market', NULL, NULL, '2021/03/26 15:11:54', 0, 'dpksharma1989@gmail.com', 'Share india', 'Uttar pardesh', 'Baraut'),
(243, 'Chandra ', 'Mohan ', '9760759325', 'Arbitrager ', NULL, NULL, '2021/03/26 15:12:44', 0, 'chandra.mohan.arb@gmail.com', 'R money', 'Up ', 'Agra '),
(244, 'Romiyo ', 'Sharma ', '9024812886', 'Salaried ', NULL, NULL, '2021/03/26 15:12:49', 0, 'romiyo_sharma@geojit.com', 'Geojit financial services ltd', 'Rajasthan ', 'Jaipur '),
(245, 'Deepak', 'Gupta', '9599808548', 'IT', NULL, NULL, '2021/03/26 15:12:49', 0, 'omdgupta@gmail.com', 'Shareindia ', 'Delhi', 'Delhi'),
(246, 'Amol ', 'Meshram', '08604737544', '', NULL, NULL, '2021/03/26 15:15:46', 0, 'amolmeshram.niam12@gmail.com', 'Balaghat', 'Madhya Pradesh', 'India'),
(247, 'Kunal', 'Shah', '7738380018', 'Head', NULL, NULL, '2021/03/26 15:16:36', 0, 'kunal.shah@nirmalbang.com', 'Nirmal bang ', 'Maharashtra ', 'Mumbai '),
(248, 'Priyanka', 'Kharche', '8788915779', 'Student', '2021/03/26 17:38:16', '2021/03/26 17:39:16', '2021/03/26 15:16:49', 0, 'kharchepriyanka6@gmail.com', 'Rahuri', 'Maharashtra', 'India'),
(249, 'mayank', 'sharma', '9212679779', 'trader', '2021/03/26 18:26:28', '2021/03/26 18:27:28', '2021/03/26 15:18:36', 0, 'mayankvyassharma@gmail.com', 'share india', 'delhi', 'delhi'),
(250, 'Sunil', 'Gupta', '9891533415', '', '2021/03/26 15:19:47', '2021/03/26 15:58:06', '2021/03/26 15:18:59', 0, 'sunilgupta03mba@gmail.com', 'Share india', 'Delhi', 'Delhi'),
(251, 'Ashwani', 'Kumar', '8097459073', 'Private Job', '2021/03/26 17:48:14', '2021/03/26 17:49:14', '2021/03/26 15:20:36', 0, 'ashwani.kumar@ncdex.com', 'NCDEX', 'Delhi', 'Delhi'),
(252, 'ANIL', 'SINGH', '9871093390', 'Service', '2021/03/26 16:56:40', '2021/03/26 16:57:40', '2021/03/26 15:21:02', 0, 'anils220785@gmail.com', '', 'Up', 'GHAZIABAD'),
(253, 'Suresh', 'Arora', '9899297218', '', '2021/03/26 15:22:44', '2021/03/26 15:23:44', '2021/03/26 15:21:04', 0, 'suresh.arora@shareindia.com', 'Delhi', 'Delhi', 'India'),
(254, 'Ravinderjit ', 'Singh', '9915722044', 'Service', '2021/03/26 15:43:01', '2021/03/26 15:44:15', '2021/03/26 15:21:44', 0, 'ravinderjit.singh@reliancefoundation.org', 'Reliance Foundation', 'Maharshtra', 'Mumbai'),
(255, 'Suresh', 'Kumar', '9810798153', 'Trader', '2021/03/26 15:24:18', '2021/03/26 15:25:18', '2021/03/26 15:21:45', 0, 'suresh_kumar@cargill.com', 'Cargill India pvt ltd', 'Delhi', 'Delhi'),
(256, 'mayank', 'sawla', '09829039195', '', NULL, NULL, '2021/03/26 15:22:27', 0, 'sawlamayank@gmail.com', 'Ramganjmandi', 'Rajasthan', 'India'),
(257, 'rajiv', 'relhan', '9820085782', 'MD', '2021/03/26 15:53:54', '2021/03/26 15:54:54', '2021/03/26 15:23:06', 0, 'rajiv.relhan@nccl.co.in', 'NCCL', 'Maharashtra', 'Mumbai'),
(258, 'Om Prakash', 'Singh', '9930388094', 'Service', '2021/03/26 15:26:04', '2021/03/26 17:56:49', '2021/03/26 15:23:21', 0, 'omprakash_dbd@yahoo.co.in', 'Anandrathi shares and Brokers Ltd', 'Maharastra', 'Mumbai'),
(259, 'Sunil', 'Nyati', '9302499999', 'Business', NULL, NULL, '2021/03/26 15:24:06', 0, 'md@swastika.co.in', 'Indore', 'India (+91)', 'Indore'),
(260, 'Dhanna Lal', 'Yadav', '9928005971', 'Service', NULL, NULL, '2021/03/26 15:24:56', 0, 'dlyadav@htplonline.com', 'Hindustan Tradecom Pvt Ltd', 'Rajasthan', 'Jaipur'),
(261, 'Avishek', 'Anand', '8779580569', 'Deputy Manager', '2021/03/26 15:34:35', '2021/03/26 17:58:09', '2021/03/26 15:25:21', 0, 'avishek.anand@ncdex.com', 'NCDEX', 'Maharashtra', 'Mumbai'),
(262, 'Ranjeet', 'Kachhwaha', '9425852002', 'CEO', NULL, NULL, '2021/03/26 15:25:25', 0, 'Kachhwaharanjeet@gmail.com', 'Kanha Krishi Vanopaj Producer Co.', 'MP', 'Mandla'),
(263, 'Rahat', 'Mehra', '7973318231', 'FInancial Market', '2021/03/26 17:02:26', '2021/03/26 17:03:26', '2021/03/26 15:25:43', 0, 'rahatmehra451@gmail.com', 'Findoc', 'Punjab', 'Ludhiana'),
(264, 'Sudhir', 'Agrawal', '8319939761', 'Business', '2021/03/26 15:26:38', '2021/03/26 15:27:38', '2021/03/26 15:25:48', 0, 'sudhiragrawal29@gmail.com', 'Kuntay oils pvt ltd dewas', 'Mp', 'Indore'),
(265, 'Priyal ', 'Pancholi', '9829139251', 'Business', '2021/03/26 15:34:02', '2021/03/26 15:35:02', '2021/03/26 15:26:01', 0, 'info_pfs@yahoo.com', 'Priyal financial services', 'Rajasthan', 'Jaipur'),
(266, 'Ritu Raj Singh', 'Rituraj Singh Rajawat', '8962324621', 'Businessman', NULL, NULL, '2021/03/26 15:26:19', 0, 'riturajsinghrajawat92@gmail.com', 'Malaw mati beej upt Sha.smt.', 'M.p', 'Tarana'),
(267, 'Deepak', 'Somani', '19826311186', 'Job', NULL, NULL, '2021/03/26 15:26:30', 0, 'deepaksomani1980@gmail.com', 'Indore', 'MADHYA PRADESH', 'India'),
(268, 'Viren', 'Mayani', '9920185402', 'service', NULL, NULL, '2021/03/26 15:26:43', 0, 'viren.mayani@kotak.com', 'kotak sec limited', 'hh', '11122'),
(269, 'mahavir', 'sharma', '9352105077', 'accountant', '2021/03/26 16:06:45', '2021/03/26 16:07:45', '2021/03/26 15:26:43', 0, 'mahavir.sharma@9star.in', 'Nine star broking Pvt Ltd', 'Rajasthan', 'jodhpur'),
(270, 'Amay', 'Sharda', '7879023905', 'Employee', NULL, NULL, '2021/03/26 15:27:00', 0, 'amay.sharda@swastika.co.in', 'Swastika Investmart', 'Madhya Pradesh', 'Indore'),
(271, 'M', 'Sharma', '9039090992', 'Service', NULL, NULL, '2021/03/26 15:27:32', 0, 'msharma@swastika.co.in', 'Swastika', 'Mp', 'Indore'),
(272, 'SRINIVASARAO', 'ADAPALA', '09618426548', '', NULL, NULL, '2021/03/26 15:27:54', 0, 'haesvesrna3@gmail.com', 'BENGALURU', 'Karnataka', 'India'),
(273, 'Vijay', 'Kedia', '9619551030', '', '2021/03/26 16:19:20', '2021/03/26 16:20:20', '2021/03/26 15:28:17', 0, 'vijay@kediacapital.com', 'Kedia Capital', 'Maharashtra', 'Thane'),
(274, 'Pratyush', 'Choudhary', '9425937224', 'Service', '2021/03/26 15:31:39', '2021/03/26 16:35:53', '2021/03/26 15:28:18', 0, 'pratyush@swastika.co.in', 'Swastika Investmart ltd', 'MP', 'Ratlam'),
(275, 'SURAJ', 'LAHINE', '9824076103', 'Relationship Manager', NULL, NULL, '2021/03/26 15:28:23', 0, 'sahine017@gmail.com', 'MARWADI SHARES AND FINANCE', 'GUJARAT', 'AHMEDABAD'),
(276, 'Shivam', 'Nigam', '08982336405', 'Branch manager', NULL, NULL, '2021/03/26 15:28:25', 0, 'imcoolshivamnigam@gmail.com', 'katni', 'madhay pradesh', 'India'),
(277, 'DIPEN', 'BHAVSAR', '9727080006', 'commodity  trader', '2021/03/26 17:03:51', '2021/03/26 17:04:51', '2021/03/26 15:28:56', 0, 'dipen.bhavsar@marwadigroup.in', 'marwadi shares and finance ltd', 'gujarat', 'ahmedabad'),
(278, 'Sanjiv', 'Kumar', '9999768466', '', '2021/03/26 17:13:40', '2021/03/26 17:56:47', '2021/03/26 15:29:43', 0, 'sanjiv.kumar@religare.com', 'Religare Broking Limited', 'UP', 'Noida'),
(279, 'Prakash', 'Matsagar', '9923689455', 'Maungiri farmer producer', NULL, NULL, '2021/03/26 15:29:53', 0, 'prakashmatsagar101@gmail.com', 'Vaijapur', 'Maharashtra', 'Vaijapur'),
(280, 'Hitesh', 'Savla', '9870107197', 'Service', NULL, NULL, '2021/03/26 15:30:03', 0, 'hitesh.savla@ncdex.com', 'NCDEX', 'Maharashtra', 'Mumbai'),
(281, 'Shivani', 'Chauhan', '6264044398', 'HR', NULL, NULL, '2021/03/26 15:30:49', 0, 'shivani.chauhan@swastika.co.in', 'Swastika', 'MP', 'Indore');
INSERT INTO `tbl_users` (`id`, `fname`, `lname`, `mobile`, `occupation`, `login_date`, `logout_date`, `joining_date`, `logout_status`, `email`, `city`, `state`, `country`) VALUES
(282, 'Malay', 'Sen', '9819069766', 'service', '2021/03/26 15:32:22', '2021/03/26 17:01:19', '2021/03/26 15:31:36', 0, 'malay.sen@ncdex.com', 'NCDEX', 'Maharashtra', 'Mumbai'),
(283, 'Ganesh', 'Ingle', '9960102581', 'Farmer', NULL, NULL, '2021/03/26 15:31:45', 0, 'ganeshingle2048@gmail.com', 'MAUNGIRI FARMER PRODUCER COMPANY', 'Maharashtra', 'Vaijapur'),
(284, 'V', 'K', '9765875731', 'Fan', NULL, NULL, '2021/03/26 15:31:58', 0, 'v@v.n', 'Hge', 'Ka', 'Blr'),
(285, 'Anita', 'Negi', '8700284002', 'Service', NULL, NULL, '2021/03/26 15:32:16', 0, 'anitanegi81@yahoo.co.in', 'Findoc', 'Delhi', 'Delhi'),
(286, 'Hitesh', 'Savla', '9870107194', 'Service', '2021/03/26 16:44:33', '2021/03/26 16:45:33', '2021/03/26 15:32:25', 0, 'hitesh.savla@ncdex.com', 'NCDEX', 'Maharashtra', 'Mumbai'),
(287, 'Gauri Shankar', 'Bajaj', '8290669301', 'AP', NULL, NULL, '2021/03/26 15:32:38', 0, 'gaurishankarbajaj@yahoo.co.in', '9star ', 'Rajasthan ', 'Jodhpur '),
(288, 'dimple', 'sharma', '8368860682', 'private job', '2021/03/26 16:35:45', '2021/03/26 16:36:45', '2021/03/26 15:32:43', 0, 'dimplesharma918@gmail.com', 'findoc investmart Pvt Ltd', 'Uttar Pradesh', 'ghaziabad'),
(289, 'Prashant ', 'Reddy ', '9910995919', 'Trader', '2021/03/26 15:59:22', '2021/03/26 16:00:22', '2021/03/26 15:32:46', 0, 'prashu1000@yahoo.co.in', 'ADM', 'Haryana', 'Delhi '),
(290, 'Harshita', 'Kavidayal', '9410172130', 'Service', '2021/03/26 16:15:38', '2021/03/26 16:16:38', '2021/03/26 15:33:07', 0, 'harshita.kavidayal@ncdex.com', 'NCDEX', 'Rajasthan', 'Jaipur'),
(291, 'Anshul', 'Mehta', '7096078825', 'Trader', NULL, NULL, '2021/03/26 15:33:32', 0, 'anshulmehta@gmail.com', '', 'Gujarat', 'Ahmedabad'),
(292, 'Pritish', 'Rohan', '8790998773', 'NCDEX', '2021/03/26 16:21:41', '2021/03/26 16:22:41', '2021/03/26 15:33:54', 0, 'pritish.rohan@ncdex.com', 'Jaipur', 'Rajasthan', 'Jaipur'),
(293, 'Poornima Shetty', 'SHETTY', '19167448895', '', NULL, NULL, '2021/03/26 15:34:13', 0, 'poornimasshetty@gmail.com', 'Mumbai', 'Maharashtra', 'India'),
(294, 'Vidyadar', 'Bangera', '09004398365', 'Job', '2021/03/26 15:36:40', '2021/03/26 15:37:40', '2021/03/26 15:34:24', 0, 'vidyadar.bangera@nccl.co.in', 'Mumbai', 'Maharashtra', 'India'),
(295, 'Nirav', 'Desai', '999331700', 'Business', '2021/03/26 15:34:59', '2021/03/26 15:35:59', '2021/03/26 15:34:26', 0, 'accoubts@nikhilgroup.co.in', 'Nikhil', 'Mp', 'Indore'),
(296, 'Nitin', 'Desai', '9004024578', 'Service', '2021/03/26 15:35:17', '2021/03/26 15:36:17', '2021/03/26 15:34:34', 0, 'nitin.desai@ncdex.com', 'NCDEX', 'Maharashtra', 'Mumbai'),
(297, 'Anita', 'Negi', '9891085477', 'Service', '2021/03/26 16:31:44', '2021/03/26 16:32:44', '2021/03/26 15:34:45', 0, 'anunegi1981.AN@gmail.com', 'Findoc', 'Delhi', 'Delhi'),
(298, 'Vijay', 'agarwal', '9440037555', 'Business', NULL, NULL, '2021/03/26 15:34:48', 0, 'Godavaricots@gmail.com', 'Adilabad', 'Telangana', 'India'),
(299, 'Atul', 'Purohit', '9669120435', 'Service Head', NULL, NULL, '2021/03/26 15:34:50', 0, 'atul.purohit@swastika.co.in', 'Swastika investmart', 'M.P', 'Indore'),
(300, 'Sachin', 'Pundle', '09920821218', 'Service', '2021/03/26 15:46:27', '2021/03/26 15:47:27', '2021/03/26 15:34:52', 0, 'sachin.pundle@nccl.co.in', 'NCCL', 'MAHARASHTRA', 'India'),
(301, 'Gaurav', 'Goyal', '9828233000', 'Sr. VP', NULL, NULL, '2021/03/26 15:34:56', 0, 'gauravgoyals@yahoo.com', 'Jaipur', 'Rajasthan', 'India'),
(302, 'Rajesh', 'Sharda', '9414127900', 'Sb', NULL, NULL, '2021/03/26 15:35:10', 0, 'rajeshsharda1@gmail.com', 'Shyam', 'Raj', 'Jodhpur'),
(303, 'Preety', 'Bawa', '8285429719', 'Job', '2021/03/26 15:35:37', '2021/03/26 15:36:37', '2021/03/26 15:35:12', 0, 'preetybawa46@gmail.com', 'Findoc investmart ltd', 'Delhi', 'Delhi'),
(304, 'Oliver', 'Samuel', '9840252030', 'Business Manager', '2021/03/26 16:01:39', '2021/03/26 16:02:39', '2021/03/26 15:35:45', 0, 'oliver.samuel@religare.com', 'Religare Broking Limited', 'Tamilnadu', 'Chennai'),
(305, 'KAPIL', 'SHARMA', '9352751859', 'Dealing Executive', NULL, NULL, '2021/03/26 15:36:12', 0, 'kapil.sharma@9star.in', 'NINE STAR BROKING PVT LTD', 'RAJASTHAN', 'JODHPUR'),
(306, 'Vinay', 'Agarwal', '9322192424', 'Salaried', '2021/03/26 15:37:55', '2021/03/26 15:38:55', '2021/03/26 15:36:22', 0, 'vinay.agarwal@choiceindia.com', 'Choice equity broking pvt ltd', 'Madhya Pradesh', 'Indore'),
(307, 'N', 'Arulvengadam', '9894607868', 'Employee ', '2021/03/26 15:37:21', '2021/03/26 15:38:21', '2021/03/26 15:36:35', 0, 'arulvengadam.n@choiceindia.com', 'Choice Equity broking ', 'Tamilnadu', 'Salem'),
(308, 'Pushkar', 'Jalal', '9811705771', 'Arbitrasie', NULL, NULL, '2021/03/26 15:36:53', 0, 'pushkarjalal66@gmail.com', 'Share india', 'Delhi', 'Delhi'),
(309, 'Raghav', 'Mundra', '9352941000', 'Stock broker', '2021/03/26 16:02:52', '2021/03/26 16:03:52', '2021/03/26 15:37:16', 0, 'raghavmundra124@gmail.com', 'Nine Star Broking Pvt Ltd', 'Rajasthan ', 'Jodhpur'),
(310, 'Deepak', 'Kumar', '9811130476', 'Trader', NULL, NULL, '2021/03/26 15:37:33', 0, 'dkmannu07@gmail.com', 'Share india', 'New delhi', 'Delhi'),
(311, 'kayomarz', 'sadri', '09820193677', '', '2021/03/26 15:39:05', '2021/03/26 15:40:05', '2021/03/26 15:37:36', 0, 'kayomarz.sadri@abans.co.in', 'mumbai', 'Maharashtra', 'India'),
(312, 'Nirmal', 'Kumar', '9828802999', 'HC', '2021/03/26 16:24:55', '2021/03/26 16:25:55', '2021/03/26 15:37:41', 0, 'nirmal.kumar@9star.in', '9star', 'Rajasthan', 'Jodhpur'),
(313, 'Mahaveer', 'Jain', '9521321080', 'Business', NULL, NULL, '2021/03/26 15:37:50', 0, 'mahaveerc@gmail.com', 'Nine Star Broking Pvt Ltd', 'Rajasthan', 'Jodhpur'),
(314, 'Bablu', 'Meghwal', '7737905162', '', '2021/03/26 15:39:35', '2021/03/26 15:40:35', '2021/03/26 15:38:18', 0, 'rajravindra2011@gmail.com', '9 star broking Pvt ltd', 'Rajasthan', 'Jodhpur'),
(315, 'Lovekesh', 'Mutreja', '8527241126', 'Trading', NULL, NULL, '2021/03/26 15:38:39', 0, 'lovekeshmutreja87@gmail.com', 'Share India', 'Delhi', 'New Delhi'),
(316, 'Ankit', 'Garg', '09045520070', 'Salaried', NULL, NULL, '2021/03/26 15:38:48', 0, 'gargankit30@gmail.com', 'Ghaziabad', 'Uttar Pradesh', 'Modinagar'),
(317, 'Kiran', 'Upadhyay', '9586288800', 'Service', NULL, NULL, '2021/03/26 15:38:49', 0, 'kiran.upadhyay2@religare.com', 'Religare Broking Ltd', 'Gujarat', 'Ahmedabad'),
(318, 'Anmol', 'Kaushal', '7307109000', 'Dealer', NULL, NULL, '2021/03/26 15:39:18', 0, 'anmol.kaushal@myfindoc.com', 'Findoc', 'Punjab', 'Ludhiana'),
(319, 'BHARAT', 'Singh', '9899881466', 'Services', '2021/03/26 15:40:25', '2021/03/26 15:41:25', '2021/03/26 15:39:25', 0, 'bharats.dl1988@gmail.com', 'Findoc investmart private limited', 'Uttar pradesh', 'Ghaziabad'),
(320, 'Amit ', 'Siyal', '9819969633', '', NULL, NULL, '2021/03/26 15:39:26', 0, 'amit.siyal@viterra.com', 'Viterra', 'MAHARASHTRA', 'Mumbai'),
(321, 'Parth', 'Nyati', '9177022699', 'CTO', NULL, NULL, '2021/03/26 15:40:20', 0, 'parth.nyati@swastika.co.in', 'Swastika Investmart Ltd', 'MP', 'Indore'),
(322, 'adhish', 'singhal', '19811798498', '', NULL, NULL, '2021/03/26 15:40:29', 0, 'adhish84@gmail.com', 'MEERUT', 'Uttar Pradesh', 'India'),
(323, 'Ravinderjit ', 'Singh', '8928524368', 'Service', NULL, NULL, '2021/03/26 15:41:55', 0, 'ravinderjits@gmail.com', 'Reliance Foundation', 'Maharshtra', 'Mumbai'),
(324, 'Nishant', 'Sharma', '9988832277', 'Manager', NULL, NULL, '2021/03/26 15:42:48', 0, 'nishantsharma99@outlook.com', 'Glenmark', 'Punjab', 'Zirakpur'),
(325, 'Sonali ', 'Padave', '9867939501', '', '2021/03/26 15:45:11', '2021/03/26 15:46:11', '2021/03/26 15:43:26', 0, 'sonali.padave@ncdex.com', 'NCDEX', 'Maharashtra', 'Mumbai'),
(326, 'Gurpreet', 'Sidana', '9810414888', 'Service ', '2021/03/26 15:58:39', '2021/03/26 17:23:12', '2021/03/26 15:43:41', 0, 'gurpreet.sidana@religare.com', 'Religare Broking Limited ', 'U.P', 'Noida '),
(327, 'Amit', 'Agarwal', '9971493263', 'Service', '2021/03/26 15:45:41', '2021/03/26 15:46:41', '2021/03/26 15:44:15', 0, 'amit.agarwal@religare.com', 'Religare Broking ', 'UP', 'Noida'),
(328, 'Brajesh', 'Agrawal', '9893058004', '', '2021/03/26 17:05:31', '2021/03/26 17:06:31', '2021/03/26 15:44:17', 0, 'brajeshag@gmail.com', 'Essence Commodities p Ltd', 'm', 'Indore'),
(329, 'aurobinda', 'prasad', '09167221247', '', NULL, NULL, '2021/03/26 15:44:48', 0, 'aurobindaprasad@gmail.com', 'Navi Mumbai', 'Maharashtra', 'India'),
(330, 'Nirav', 'Raval', '7977351515', 'Business', NULL, NULL, '2021/03/26 15:44:56', 0, 'mr.niravraval@gmail.com', 'Kalpana Enterprises', 'Gujarat', 'Vadodara'),
(331, 'Rajesh', 'Dhasmana', '9711338028', 'Trading', NULL, NULL, '2021/03/26 15:45:06', 0, 'raj.rajdhasmana@gmail.com', 'ShareIndia', 'Haryana', 'Faridabad'),
(332, 'Mahesh', 'Kumar', '8459495011', 'Trader', NULL, NULL, '2021/03/26 15:45:45', 0, 'Kamalkmr131@gmail.com', 'Raghu Nandan Money', 'New Delhi', 'Delhi'),
(333, 'Rajini', 'Panicker', '9820131530', 'Private Services ', '2021/03/26 16:13:07', '2021/03/26 17:21:24', '2021/03/26 15:46:47', 0, 'rpanicker@phillipcapital.in', 'PhillipCapital', 'Maharashtra ', 'Mumbai '),
(334, 'Shailesh', 'Baldha', '9727721406', 'Service', NULL, NULL, '2021/03/26 15:47:01', 0, 'shailesh.baldha@adaniwilmar.in', 'AHMEDABAD', 'GUJARAT', 'India'),
(335, 'Manish', 'Thool', '9403639205', 'President', '2021/03/26 16:04:17', '2021/03/26 16:04:34', '2021/03/26 15:48:01', 0, 'manishdthool@gmail.com', 'Dharasatva FPC', 'Maharastra', 'Giroli'),
(336, 'Dharmendra', 'Sharma', '8487881184', '', NULL, NULL, '2021/03/26 15:49:07', 0, 'dharmendra9555@gmail.com', '', 'Delhi', 'Delhi'),
(337, 'Kanchan ', 'Jain ', '7796200852', 'Job', NULL, NULL, '2021/03/26 15:50:18', 0, 'jain.kanchan@religare.com', 'Religare Broking ', 'Maharashtra ', 'Jalgaon '),
(338, 'Sunandh', 'Subramaniam', '9987199398', 'Senior Research Analyst', '2021/03/26 16:00:03', '2021/03/26 16:01:03', '2021/03/26 15:50:21', 0, 'sunand.subramaniam@gmail.com', 'Choice Equity Broking', 'Maharashtra', 'Mumbai'),
(339, 'Avneet', 'Sharma', '9811193538', 'Service', NULL, NULL, '2021/03/26 15:50:31', 0, 'avneetansh@gmail.com', 'Share india ', 'Delhi', 'Delhi'),
(340, 'Anshul', 'Joshi', '9643582399', 'Manager', NULL, NULL, '2021/03/26 15:50:34', 0, 'joshianshul96@gmail.com', 'Religare', 'Maharashtra', 'Pune'),
(341, 'Hanuman', 'Pokale', '9421988589', 'CMD', '2021/03/26 15:52:23', '2021/03/26 15:53:23', '2021/03/26 15:50:57', 0, 'hanupokale@gmail.com', 'Yuvakmangal Agrotech Farmers Producer Company Ltd', 'Maharashtra', 'India'),
(342, 'Prashant ', 'Rai', '8857822494', 'Service', '2021/03/26 15:52:06', '2021/03/26 15:53:06', '2021/03/26 15:51:06', 0, 'Rai.prashant@religare.com', 'Religare ', 'Maharashtra ', 'Pune'),
(343, 'Dinesh', 'Goyal', '9899000804', 'Arbitrase', '2021/03/26 15:53:28', '2021/03/26 15:54:51', '2021/03/26 15:52:13', 0, 'dinesh804goyal@gmail.com', 'Share India capital Ltd.', 'Delhi', 'Delhi'),
(344, 'vimla', 'Kurma', '9167012323', 'service', '2021/03/26 15:53:23', '2021/03/26 17:52:56', '2021/03/26 15:52:55', 0, 'vimla.kurma@ncdex.com', 'NCDEX', 'Maharashtra', 'Mumbai'),
(345, 'smita', 'chaudhary', '9833718150', '', NULL, NULL, '2021/03/26 15:53:23', 0, 'smitu.chaudhary@gmail.com', 'ncdex', 'maharashra', 'mumbai'),
(346, 'Naresh', 'Dhoka', '09449211177', 'Business ', NULL, NULL, '2021/03/26 15:55:20', 0, 'nareshdhoka@gmail.com', 'Raichur', 'Karnataka', 'India'),
(347, ' Shamsher', 'Singh', '8700822364', '', NULL, NULL, '2021/03/26 15:56:26', 0, 'shamsherbali1983@gmail.com', 'Raghunandan money', 'Delhi', 'Delhi'),
(348, 'Pankaj kumar', 'sharma', '9999686836', '', '2021/03/26 16:04:17', '2021/03/26 16:05:17', '2021/03/26 15:56:55', 0, 'Pksharma1984mailbox@gmail.com', 'Share india', 'DELHI', 'delhi'),
(349, 'Vivek', 'Singh', '9818697159', 'Service', NULL, NULL, '2021/03/26 16:00:00', 0, 'vivekrajputa7@gmail.com', 'Share india securities Ltd', 'Delhi', 'Delhi'),
(350, 'Sugandha', 'Sachdeva', '09654862707', 'VP', NULL, NULL, '2021/03/26 16:01:58', 0, 'sugandha.sachdeva@religare.com', 'Noida', 'U.P.', 'India'),
(351, 'Pooja', 'Singh', '9769048223', '', '2021/03/26 18:00:13', '2021/03/26 18:01:13', '2021/03/26 16:01:59', 0, 'pooja.singh@ncdex.com', 'NCDEX', 'Maharashtra', 'Mumbai'),
(352, 'Vipul', 'Pabari', '9825933970', 'Salaried', '2021/03/26 16:05:28', '2021/03/26 16:06:28', '2021/03/26 16:03:29', 0, 'vipul.pabari@marwadionline.in', 'MSFL', 'Gujrat', 'Rajkot'),
(353, 'Vikram', 'Udeshi', '09820019476', '', '2021/03/26 16:06:16', '2021/03/26 16:07:16', '2021/03/26 16:04:06', 0, 'vikramudeshi@gmail.com', 'Mumbai', 'Maharashtra', 'India'),
(354, 'Amit', 'Baid', '9953194421', 'Arbitrage', NULL, NULL, '2021/03/26 16:07:43', 0, 'amitbaidsadulpur@gmail.com', 'Share India', 'Delhi', 'Delhi'),
(355, 'Manish', 'Daga', '19820072705', 'FPO', NULL, NULL, '2021/03/26 16:08:24', 0, 'prdcottons@gmail.com', 'MUMBAI', '400080', 'India'),
(356, 'Runwal', 'Complex', '09820072705', 'FPO', NULL, NULL, '2021/03/26 16:08:37', 0, 'accounts@cottonguru.org', 'MUMBAI', 'MAHARASHTRA', 'India'),
(357, 'Pardeep', 'Khokra', '9813657325', 'Share market ', NULL, NULL, '2021/03/26 16:10:16', 0, 'parveshkumar396@gmail.com', 'Share india', 'Haryana', 'Kurukshetra'),
(358, 'Sanjay', 'Namdev', '9301143877', 'Soyabeen', NULL, NULL, '2021/03/26 16:10:59', 0, 'erdafarmer@gmail.com', 'Erda farmers producer company ltd.', 'M.p.', 'Sehore'),
(359, 'Surendra ', 'Babu', '9900669800', 'Oil mill', NULL, NULL, '2021/03/26 16:11:20', 0, 'indrakolla@yahoo.co.in', 'SRI LAXMISRINIVASA OIL MILL', 'Karnataka ', 'Gangavathi '),
(360, 'Pankaj', 'Sharma', '9013079796', 'Trader', NULL, NULL, '2021/03/26 16:13:29', 0, 'pankajsharmakaku@gmail.com', 'Raghunandan money', 'New delhi', 'India'),
(361, 'Kamal', 'Mishra', '8770581165', 'Fpo', '2021/03/26 17:07:21', '2021/03/26 17:08:21', '2021/03/26 16:14:22', 0, 'Kamalmishra90@gmail.com', 'Rewanchal pulse producer company ltd ', 'MADHYA PRADESH', 'Rewa'),
(362, 'Sarvesh', 'Singh', '6263196675', '', '2021/03/26 16:19:17', '2021/03/26 16:20:17', '2021/03/26 16:14:48', 0, 'vcpclk@gmail.com', 'Vindhyachal crop producer company LTD', 'Madhya Pradesh', 'Rewa'),
(363, 'Harvinder', 'Singh', '9971144520', 'AVP', '2021/03/26 16:15:38', '2021/03/26 16:16:38', '2021/03/26 16:15:02', 0, 'ca.harvinder@gmail.com', 'NCDEX', 'Delhi', 'Delhi'),
(364, 'Krishnadhar ', 'Dwivedi', '7000487317', 'Fpo', '2021/03/26 17:05:46', '2021/03/26 17:06:46', '2021/03/26 16:15:50', 0, 'vcpclk@gmail.com', 'Vindhyanchal crop producer company ltd', 'MADHYA PRADESH', 'Rewa'),
(365, 'Dinesh Kumar ', 'Dwivedi ', '9977719221', 'Service ', '2021/03/26 16:18:35', '2021/03/26 16:19:35', '2021/03/26 16:17:37', 0, 'dwivediciae@gmail.com', 'Kvk ', 'M P', 'Bhopal '),
(366, 'Nidhi ', 'Pandey', '9406904769', 'Job', '2021/03/26 16:19:06', '2021/03/26 16:20:06', '2021/03/26 16:18:36', 0, 'pckarnavati@rediffmail.com', 'FPO', 'M.P.', 'Panna'),
(367, 'Daniel', 'A', '9500691658', 'Private', '2021/03/26 16:19:18', '2021/03/26 16:20:18', '2021/03/26 16:19:09', 0, 'daniel.a@tafe.com', 'TAFE', 'TN', 'Chennai'),
(368, 'Rishabh', 'Sharma', '8860217034', 'Trader ', NULL, NULL, '2021/03/26 16:19:33', 0, 'sharmarishabh098@gmail.com', 'Share India securities', 'Delhi ', 'Delhi '),
(369, 'Abhishek', 'Agarwal', '9307788840', '', NULL, NULL, '2021/03/26 16:21:43', 0, 'abhishek@achintya.co.in', 'Achintya Commodities Private Limited', 'Uttar Pradesh', 'Kanpur'),
(370, 'Manish', 'Patel', '9850820909', 'Farmer', '2021/03/26 16:23:54', '2021/03/26 16:24:54', '2021/03/26 16:22:47', 0, 'rmanish2000@yahoo.co.in', '', 'Madhya pradesh', 'Dobhi'),
(371, 'HARDIK', 'DHINGRA', '7891657936', '', '2021/03/26 16:27:22', '2021/03/26 16:28:22', '2021/03/26 16:26:19', 0, 'hardikdhingra009@gmail.com', 'Panipat', 'Haryana', 'pnp'),
(372, 'Arpit', 'Agarwal', '8874466000', '', '2021/03/26 16:30:17', '2021/03/26 16:31:17', '2021/03/26 16:28:24', 0, 'arpit@achintya.co.in', 'Achintya Commodities Pvt Ltd', 'UP', 'kanpur'),
(373, 'Tarun', 'Aggarwal', '9999355225', 'Arbitrage', NULL, NULL, '2021/03/26 16:32:59', 0, 'tarun.kumar20x@gmail.com', 'Share india private limited', 'Delhi', 'New delhi'),
(374, 'Mrituenjay', 'Jha', '9999896456', 'Journalism', NULL, NULL, '2021/03/26 16:33:16', 0, 'mrityunjay.kumarjha@zeemedia.esselgroup.com', 'Zee Media', 'Maharashtra', 'Mumbai'),
(375, 'Tanushree', 'Dave', '9930168521', '', NULL, NULL, '2021/03/26 16:33:37', 0, 'tanushree.dave@gmail.com', 'Udaipur', 'RAJASTHAN', 'Udaipur'),
(376, 'Abhishek', 'Agarwal', '9307788850', 'Business ', '2021/03/26 16:34:34', '2021/03/26 16:35:34', '2021/03/26 16:34:06', 0, 'abhishek@achintya.co.in', 'Achintya', 'Uttar pradesh', 'Kanpur'),
(377, 'Brajesh ', 'Sharma ', '9926390754', 'Service', '2021/03/26 16:45:20', '2021/03/26 16:46:20', '2021/03/26 16:44:56', 0, 'bkpclbina@gmail.com', 'Bina krishak producer company limited ', 'Madhya Pradesh', 'Bina'),
(378, 'Ranjeet', 'Begwani', '9811687600', '', NULL, NULL, '2021/03/26 16:51:42', 0, 'ranjeetbegwani@yahoo.com', 'Share India ', 'Delhi', 'Delhi '),
(379, 'ashok', 'yadav', '7000135409', 'commodity', '2021/03/26 17:00:37', '2021/03/26 17:01:37', '2021/03/26 16:59:09', 0, 'ashok.india889@gmail.com', 'PRITHVI FINMART PVT LTD', 'MADHYA PRADESH', 'indore'),
(380, 'Mohit', 'Barolia', '9610388999', 'Salaried', '2021/03/26 17:00:13', '2021/03/26 17:01:13', '2021/03/26 16:59:44', 0, 'mohit.barolia@ncdex.com', 'NCDEX Ltd.', 'New Delhi', 'New Delhi'),
(381, 'Murlidhar', 'Dwivedi', '6260344258', 'Fpo', '2021/03/26 17:08:51', '2021/03/26 17:09:51', '2021/03/26 17:02:38', 0, 'mdwivedi41@gmail.com', 'Rampur milk producer company ltd', 'MADHYA PRADESH', 'Satna'),
(382, 'Manmohan ', 'Dubey', '8720020030', 'Seeds Business ', '2021/03/27 13:19:08', '2021/03/27 13:20:08', '2021/03/26 17:03:42', 0, 'gmcpcl2014@gmail.com', 'Gurudev Manila Crop Producer Company Limited', 'Madhya Pradesh', 'Tikamgarh'),
(383, 'Varun', 'Vashisht', '9814644678', 'Accountant', '2021/03/26 17:04:47', '2021/03/26 17:05:47', '2021/03/26 17:04:02', 0, 'vashishtvarun123@gmail.com', 'Findoc group ', 'Punjab ', 'Ludhiana'),
(384, 'Shantilaxmi', 'Prabhu', '9867124310', 'National Clearing Corporation Ltd', '2021/03/26 17:08:47', '2021/03/26 17:09:47', '2021/03/26 17:07:34', 0, 'shantilaxmi.prabhu@nccl.co.in', 'Mumbai', 'Maharashtra', 'India'),
(385, 'Nagina', 'Gowda', '9870025891', 'NCCL', '2021/03/26 17:08:48', '2021/03/26 17:09:48', '2021/03/26 17:07:38', 0, 'nagina.gowda@nccl.co.in', 'NCCL', 'Maharashtra', 'Mumbai'),
(386, 'Suresh', 'Devnani', '9969208865', 'Service', '2021/03/26 17:45:13', '2021/03/26 17:46:13', '2021/03/26 17:44:38', 0, 'Suresh.devnani@samunnati.com', 'Samunnati', 'Maharashtra', 'Mumbai'),
(387, 'Deepak kumar', 'Bansiya', '8839157794', 'Farmer', NULL, NULL, '2021/03/26 18:01:25', 0, 'dbansiya@gmail.com', 'Fpo', 'Madhya Pradesh', 'Susner'),
(388, 'Dnyaneshwar', 'Dhekade', '7588762101', 'Farmer', '2021/03/26 18:16:21', '2021/03/26 18:17:21', '2021/03/26 18:12:36', 0, 'pariwartan170183@gmail.com', 'Pariwartan organic farmers producer company ltd', 'Maharashtra', 'Washim'),
(389, 'Rajesh', 'Malhotra', '09818180857', 'Self employed', NULL, NULL, '2021/03/26 18:12:41', 0, 'rajeshmalhotra01979@gmail.com', 'NEW DELHI', 'DELHI', 'Delhi'),
(390, 'Dr Prakash', 'Rajguru', '9822186940', 'Farmer', NULL, NULL, '2021/03/26 18:33:56', 0, 'prakash_rajgure@yahoo.com', 'Hingoli', 'MAHARASHTRA', 'Hingoli'),
(391, 'PRAVEEN ', 'Baid', '9560262545', 'Survive', NULL, NULL, '2021/03/26 20:26:53', 0, 'p.kmr9560@gmail.com', 'Shar india', 'New Delhi', 'New Delhi'),
(392, 'Manish', 'Daga', '9820072705', 'FPO', '2021/03/26 20:54:23', '2021/03/26 20:55:23', '2021/03/26 20:54:13', 0, 'prdcottons@gmail.com', 'MUMBAI', '400080', 'India'),
(393, 'sujata', 'nakat', '07666497194', 'Executive Director Fpc', NULL, NULL, '2021/03/26 21:05:38', 0, 'sujatavnakat@gmail.com', 'Akola', 'Maharashtra', 'India'),
(394, 'Sumit ', 'Begwani', '8178650203', 'Arbite', NULL, NULL, '2021/03/26 21:47:03', 0, 'rk7020234@gmail.com', 'Ragunandan', 'Delhi', 'Delhi'),
(395, 'VIKRAM', 'KUDALE', '09975839314', 'Director ', NULL, NULL, '2021/03/27 16:15:22', 0, 'Vikramkudale1@gmail.com', 'Jejuri', 'MAHARASHTRA ', 'India'),
(396, 'Arjunsingh', 'Vaghela', '9909793047', 'Farmer', NULL, NULL, '2021/03/28 14:19:38', 0, 'arjunsingh.vaghela@gmail.com', 'DevGadh farm', 'Gujarat', 'Deodar'),
(397, 'Gopal', 'agrawal ', '9822474468', 'oil mill', NULL, NULL, '2021/03/29 17:18:47', 0, 'agrawalshasha@gmail.com', 'ambika industries ', 'maharashtra ', 'akot'),
(398, 'shah', 'dharmen', '9824077178', 'broking', '2021/03/30 14:05:59', '2021/03/30 14:06:59', '2021/03/30 13:48:04', 0, 'dharmen@dalalkantilal.in', 'dalal kantilal & company', 'gujarat', 'amedabad'),
(399, 'Binod', 'anand', '9767561212', 'Professional', NULL, NULL, '2021/03/30 21:38:42', 0, 'cnrigreenworld@gmail.com', 'CNRI', 'NEw Delhi', 'New Delhi'),
(400, 'Yogesh', 'Sharma', '7828389978', 'Owner', '2021/04/01 19:15:10', '2021/04/01 19:16:10', '2021/04/01 19:06:15', 1, 'sharmaoilindustries2015@gmail.com', 'Sharma Oil Industries', 'Madhya pradesh', 'Dewas'),
(401, 'PAWAN', 'SHILWANT', '09545329222', 'iiiyy', NULL, NULL, '2021/07/13 10:16:42', 0, 'pawan@coact.co.in', 'Bangalore', 'Karnataka', 'India'),
(402, 'r', 'g', '9876543210', 'CH', NULL, NULL, '2021/07/13 10:46:44', 0, 'roohit@rohitgeorge.in', 'RG', 'K', 'Blor'),
(403, 'r', 'g', '09819851383', '', '2021/07/13 10:48:39', '2021/07/13 12:50:53', '2021/07/13 10:48:09', 1, 'rohit@rohitgeorge.in', 'No', 'Tamil Nadu', 'India'),
(404, 'Jagadish', 'r', '9743598479', 'BDE', '2021/09/06 10:33:45', '2021/09/06 10:33:54', '2021/09/06 10:25:20', 0, 'jagadish@coact.co.in', 'CoAct', 'Karnataka', 'Bangalore');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_pollanswers`
--
ALTER TABLE `tbl_pollanswers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_polls`
--
ALTER TABLE `tbl_polls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_reactions`
--
ALTER TABLE `tbl_reactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_pollanswers`
--
ALTER TABLE `tbl_pollanswers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_polls`
--
ALTER TABLE `tbl_polls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_reactions`
--
ALTER TABLE `tbl_reactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=405;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
