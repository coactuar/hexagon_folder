<?php
	require_once "config.php";
	
	if(!isset($_SESSION["user_id"]))
	{
		header("location: index.php");
		exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            

            unset($_SESSION["name"]);
            unset($_SESSION["user_id"]);
            
            header("location: index.php");
            exit;
        }

    }
	
?>


<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Schaeffler</title>

        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
       

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/fontAwesome.css">

		<link rel="stylesheet" href="css/templatemo-style-vid.css">
		
		<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/clappr@latest/dist/clappr.min.js"></script>
       
		<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
       
    </head>

<body>
<img src="img/topbanner.jpg" width="100%">
	<div class="board">
	

		<section class="banner" id="top">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-9">
						<div class="left-banner-content">
						<div class="embed-responsive embed-responsive-16by9 botom">
								<iframe class="embed-responsive-item" src="player.php" allowfullscreen scrolling="no"></iframe>
							</div>	
							<div class="imgresponsive">
								<img src="img/videobanner.jpg" width=100%>
							</div>
								
						</div>
					</div>
					<div class="col-md-3">
						<div class="right-banner-content">

						 <!-- <div class="login pull-right">
							<div class="card ">
								<div class="header">
									<div class="pull-left ">
									<h5 class="card-title">Live Poll</h5>
									</div>
									<div class="pull-right">
									<h5 class="card-title">1000 <i class="fa fa-users" aria-hidden="true"></i></h5>
									</div>
								</div>
								<br/>
								<br/>
								<div class="card-body">
									This is some text within a card body.
								</div>
							</div>
						</div>   -->

							<div class="login botom-form pull-right" >
							
									<div id="togle" class="none">
										<div id="message" style="display:none;"></div>	
											<form id="question-form" method="post" role="form" >
											<input type="hidden" id="name" name="name" value="<?php echo $_SESSION['name']; ?>">
												<div class="form-group">
													<textarea name="userQuestion" id="userQuestion" placeholder="Please ask a question?|"></textarea>
												</div>
												<div class="form-group ">
													<button type="submit" class="btn-secondary sub">Submit </button>
												</div>
											
											</form>
									</div>
								
									<button onclick="togle()" class="btn btn-success col-md-12 ">
									Ask a question ?
									</button>
							
							</div>
							
						</div>
						<a href="?action=logout" class="logout">
						
							Log out
						
						</a>
					</div>
				</div>
			</div>
		</section>
	</div>
<script>
//togle function
function togle() {
   var element = document.getElementById("togle");
   element.classList.toggle("togle");
}

//submit function
	$(function(){

		$(document).on('submit', '#question-form', function()
		{  
				$.post('submitques.php', $(this).serialize(), function(data)
				{
					if(data=="success")
					{
					  $('#message').removeClass('alert-danger fail');
					  $('#message').addClass('alert-success success'); 
					  $('#message').text('Your question is submitted successfully.').fadeIn().delay(2000).fadeOut();
					  $('#question-form').find("textarea").val('');
					}
					else 
					{
					  $('#message').addClass('alert-danger fail');
					  $('#message').removeClass('alert-success success'); 
					  $('#message').text(data).fadeIn().delay(3000).fadeOut();;
					}
					
				});
			
		  
		  return false;
		});
	});
	
</script>
<script>
if ($(window).width() < 628) {
  $('div').removeClass('botom');
}
if ($(window).width() < 985) {
	$('div').removeClass('left-banner-content');
}
</script>

   
    
    
</body>
</html>