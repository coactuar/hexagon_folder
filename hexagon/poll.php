<?php
require_once "config.php";

$query = "select * from tbl_pollanswers where poll_id = '".$_GET['id']."' and users_id='".$_SESSION['user_id']."'";
$res = mysqli_query($link, $query) or die(mysqli_error($link)); 
if (mysqli_affected_rows($link) > 0) 
{
    header("location: pollresults.php?id=".$_GET['id']);
	exit;
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Polls</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body class="no-bg">
<div class="container-fluid">
    <div class="row pollques border">
        <div class="col-12">
               <?php
                  if(isset($_GET['id']) && !empty($_GET['id'])) 
                  {
                      $qid = $_GET['id'];
                      $query="select * from tbl_polls where id='$qid'";
                      $res = mysqli_query($link, $query) or die(mysqli_error($link));
                      if (mysqli_affected_rows($link) > 0) 
                      {
                          $data = mysqli_fetch_assoc($res);
                          $id = $data['id'];
                      
                      ?>
                       <!-- <div class="login pull-right">
							<div class="card ">
								<div class="header">
									<div class="pull-left ">
									<h5 class="card-title">Live Poll</h5>
									</div>
									<div class="pull-right">
									<h5 class="card-title">1000 <i class="fa fa-users" aria-hidden="true"></i></h5>
									</div>
								</div>
								<br/>
								<br/>
								<div class="card-body">
									This is some text within a card body.
								</div>
							</div>
            </div>   -->
                    
                      <h6>Live Poll</h6>
                     
                     <form method="POST" action="submitpoll.php" class="form panel-body">
                          <div class="form-group">
                           <b><?php echo $data['poll_question']; ?></b>   
                          </div>
                          <hr/>
                          <div class="form-group">
                            <div class="custom-control custom-radio">
                              <input type="radio" id="pollopt1" name="pollopts" class="custom-control-input" value="opt1">
                              <label class="custom-control-label pad" for="pollopt1"><?php echo $data['poll_opt1']; ?></label>
                            </div>
                            <div class="custom-control custom-radio">
                              <input type="radio" id="pollopt2" name="pollopts" class="custom-control-input" value="opt2">
                              <label class="custom-control-label pad" for="pollopt2"><?php echo $data['poll_opt2']; ?></label>
                            </div>
                            <div class="custom-control custom-radio">
                              <input type="radio" id="pollopt3" name="pollopts" class="custom-control-input" value="opt3">
                              <label class="custom-control-label pad" for="pollopt3"><?php echo $data['poll_opt3']; ?></label>
                            </div>
                            <div class="custom-control custom-radio">
                              <input type="radio" id="pollopt4" name="pollopts" class="custom-control-input" value="opt4">
                              <label class="custom-control-label pad" for="pollopt4"><?php echo $data['poll_opt4']; ?></label>
                            </div>
                          </div>
                          <input type="hidden" id="pid" name="pid" value="<?php echo $data['id']; ?>">
                          <div class="pollbtn">
                          <button class="btn btnpoll" type="submit">Submit</button>
                          </div>
                      </form>
                      <?php  
                          //echo $id;
                      }     
                  }
              ?>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script>
if ($(window).width() < 540) {
  $('div').removeClass('row');
}
</script>
</body>
</html>