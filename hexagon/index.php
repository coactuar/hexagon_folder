<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Schaeffler</title>

        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
      

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/fontAwesome.css">

        <link rel="stylesheet" href="css/templatemo-style.css">

       <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

       <style>
/* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

/* Firefox */
input[type=number] {
  -moz-appearance: textfield;
}
</style>
    </head>

<body>
	
	<div class="board">
		<section class="banner" id="top">
			<div class="container-fluid">
			
				<div class="row">
					<div class="col-md-8">
						<div class="left-banner-content">
							<div class="imgresponsive">
								<img src="img/fsbg.jpg" width=100%>
							</div>
						
						</div>
					</div>
					<div class="col-md-4">
						<div class="right-banner-content">
							<div class="whitebg">
								<img src="img/headertext.png" width=100% >
							  <!-- <h1 class="fpheading"><b>schaeffler</b></h1>
								<p class="fpparagraph"><b>industrial distributor day, india <br/>23rd september 2020</b></p> -->
									<div class="login">
										<h3 class="loginparagraph">
										<img src="img/formtext.png" width=75% >
										</h3>
											<form id="login-form" method="post" role="form" >
												<div id="login-message"></div>
												<div class="form-group">
													<input class="loginform" type="text" aria-label="Name" aria-describedby="basic-addon1"  name="name" id="name" placeholder="Name">
												</div>
												<div class="form-group">
													<input class="loginform" type="number" aria-label="Mobile" aria-describedby="basic-addon1" name="mobile" id="mobile" placeholder="Mobile">
												</div>
												<div class="form-group">
													<input class="loginform" type="text" name="accode" aria-label="Accode" aria-describedby="basic-addon1" id="accode" placeholder="Access code">
												</div>
												<div class="form-group ">
													<button id="submit" class="btn btn-new" type="submit" >Log In &nbsp <i class="fa fa-chevron-right" aria-hidden="true"></i></button>
												</div>
											
											</form>
									</div>

							</div>
						   
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>


	  
<script>
if ($(window).width() < 1734) {
  $('div').removeClass('login');
}

</script>
   
<script>
	$(document).on('submit', '#login-form', function()
	{  
	  $.post('chforlogin.php', $(this).serialize(), function(data)
	  {
		  console.log(data);
		  if(data == 's')
		  {
			window.location.href='video.php';  
		  }
		  else if (data == '-1')
		  {
			  $('#login-message').text('You are already logged in. Please logout and try again.');
			  $('#login-message').addClass('alert-danger').fadeIn().delay(3000).fadeOut();
			
			  return false;
		  }
		  else if (data == '0')
		  {
			  $('#login-message').text('Please contact Hexagon for support');
			  $('#login-message').addClass('alert-danger').fadeIn().delay(3000).fadeOut();
			
			  return false;
		  }
		  else
		  {
			  $('#login-message').text(data);
			 $('#login-message').addClass('alert-danger').fadeIn().delay(3000).fadeOut();
			 
			  return false;
		  }
	  });
	  
	  return false;
	});
	</script> 
    
</body>
</html>