<?php
require_once "../config.php";
if(!isset($_SESSION["admin_user"]))
	{
		header("location: index.php");
		exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            unset($_SESSION["admin_user"]);
            
            header("location: index.php");
            exit;
        }

    }
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Poll Results</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body class="admin">
<div class="container-fluid">
    <div class="row">
        <div class="col-12 col-md-2">
            <img src="../img/hexagon-live.png" class="img-fluid" alt=""/> 
        </div>
    </div>      
     <div class="row bg-dark p-1 mt-1">   
        <div class="col-8 text-left">
            <a href="users.php">Users</a> | <a href="questions.php">Questions</a> | <a href="pollsQuestions.php">Polls</a> | <a href="settings.php">Settings</a>
        </div>
        <div class="col-4 text-right">
            <a href="#">Hello, <?php echo $_SESSION["admin_user"]; ?>!</a> <a href="?action=logout">Logout</a>
        </div>
    </div>
    <div class="row pollques mt-2 p-2">
        <div class="col-12 col-md-6 p-1 border">
            <h6>Poll Results</h6>
            <form>
            <div id="pollresults"></div>
            </form>
        </div>
        <div class="col-12 col-md-3 p-1 border">
            <h6>Poll Answer Times</h6>
            <form>
            <div id="polltimes"></div>
            </form>
        </div>
        <div class="col-12 col-md-3 p-1 border">
            <h6>Scores</h6>
            <form>
            <div id="pollscores"></div>
            </form>
        </div>
        
     </div> 
     
</div>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script language="javascript">
$(function(){
    getPollResults("<?php echo $_GET['id']; ?>");
    getPollTimes("<?php echo $_GET['id']; ?>");
    getPollScores();
});

function getPollResults(id)
{
    $.ajax({
        url: 'ajax.php',
        data: {action: 'getpollresults', pid: id},
        type: 'post',
        success: function(response) {
            
            $("#pollresults").html(response);
            
        }
    });
    
}
function getPollTimes(id)
{
    $.ajax({
        url: 'ajax.php',
        data: {action: 'getpolltimes', pid: id},
        type: 'post',
        success: function(response) {
            
            $("#polltimes").html(response);
            
        }
    });
    
}

function getPollScores()
{
    $.ajax({
        url: 'ajax.php',
        data: {action: 'getpollscores'},
        type: 'post',
        success: function(response) {
            
            $("#pollscores").html(response);
            
        }
    });
    
}


setInterval(function(){ getPollResults('<?php echo $_GET['id']; ?>'); }, 3000);
setInterval(function(){ getPollTimes('<?php echo $_GET['id']; ?>'); }, 5000);
setInterval(function(){ getPollScores(); }, 5000);
</script>
</body>
</html>