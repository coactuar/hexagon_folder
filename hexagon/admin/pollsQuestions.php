<?php
	require_once "../config.php";
	
	if(!isset($_SESSION["admin_user"]))
	{
		header("location: index.php");
		exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            unset($_SESSION["admin_user"]);
            
            header("location: index.php");
            exit;
        }

    }
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Poll Questions</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">

</head>

<body class="admin">

<div class="container-fluid">
    <div class="row">
        <div class="col-12 col-md-2">
            <img src="../img/hexagon-live.png" class="img-fluid" alt=""/> 
        </div>
    </div>      
     <div class="row bg-dark p-1 mt-1">   
        <div class="col-8 text-left">
            <a href="users.php">Users</a> | <a href="questions.php">Questions</a> | <a href="pollsQuestions.php">Polls</a> | <a href="settings.php">Settings</a>
        </div>
        <div class="col-4 text-right">
            <a href="#">Hello, <?php echo $_SESSION["admin_user"]; ?>!</a> <a href="?action=logout">Logout</a>
        </div>
    </div>
    
   <div class="row mt-2 p-2">
        <div class="col-12">
            <div id="poll"> </div>
        </div>
   </div>
    
</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(function(){
getPolls('1');
});

function update(pageNum)
{
  getPolls(pageNum);
}

function getPolls(pageNum)
{
    $.ajax({
        url: 'ajax.php',
        data: {action: 'getpolls', page: pageNum},
        type: 'post',
        success: function(response) {
            $("#poll").html(response);
            
        }
    });
    
}

function updatePoll(id, val)
{
    var pid = '#poll'+id;
    //var curval = $(pid).val(); 
    $.ajax({
        url: 'ajax.php?id=' + id,
         data: {action: 'updatepoll', val: val },
         type: 'post',
         success: function(output) {
             //alert(output);
             getPolls('1');
         }
   });   
}

function delPoll(id)
{
    if(confirm('Are you sure?')){
        $.ajax({
            url: 'ajax.php',
             data: {action: 'delpoll', poll: id },
             type: 'post',
             success: function(output) {
                 //alert(output);
                 getPolls('1');
             }
       });
    }
}

</script>
</body>
</html>