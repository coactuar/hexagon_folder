<?php
require_once "../config.php";

if(isset($_POST['action']) && !empty($_POST['action'])) {
    
    $action = $_POST['action'];
    
    switch($action) {
        
        
        case 'getusers':
        
            if (isset($_POST["page"])) 
            { 
                $page  = $_POST["page"]; 
            }
            else { 
                $page=1; 
            }
            
            $start_from = ($page-1) * $limit;
        
            $sql = "SELECT COUNT(id) as count FROM tbl_users";  
            $rs_result = mysqli_query($link, $sql) or die(mysqli_error($link)); 
            $row = mysqli_fetch_assoc($rs_result);
            $total_records = $row['count'];  
            $total_pages = ceil($total_records / $limit);
            ?>
            <div class="row user-info">
                <div class="col-6">
                    Total Users: <?php echo $total_records; ?>
                </div>
            </div> 
            <div class="row user-details">
                <div class="col-12">
                    <table class="table table-striped table-light">
                      <thead class="thead-inverse">
                        <tr>
                          <th>Name</th>
                          <th>Registered On</th>
                          <th>Last Login On</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php		
                        $query="select * from tbl_users order by login_date desc LIMIT $start_from, $limit";
                        $res = mysqli_query($link, $query) or die(mysqli_error($link));
                        while($data = mysqli_fetch_assoc($res))
                        {
                        ?>
                          <tr>
                            <td><?php echo $data['name']; ?></td>
                            <td><?php 
                                if($data['joining_date'] != ''){
                                    $date=date_create($data['joining_date']);
                                    echo date_format($date,"M d, H:i a"); 
                                }
                                else{
                                    echo '-';
                                }
                                ?>
                            </td>
                            <td><?php 
                                if($data['login_date'] != ''){
                                    $date=date_create($data['login_date']);
                                    echo date_format($date,"M d, H:i a"); 
                                }
                                else{
                                    echo '-';
                                }
                                ?>
                            </td>
                          </tr>
                      <?php			
                        }
                      ?>
                  
                    </table>  
                </div>
            </div>   
            <nav>
              <ul class="pagination pagination-sm" id="pagination">
                <?php if(!empty($total_pages)):for($i=1; $i<=$total_pages; $i++):  
                            if($i == 1):?>
                     <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php else:?>
                    <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php endif;?>
                <?php endfor;endif;?>
              </ul>
            </nav>
            <?php
        
            
        break;
        
        case 'getquestions':
        
            if (isset($_POST["page"])) 
            { 
                $page  = $_POST["page"]; 
            }
            else { 
                $page=1; 
            }
            
            $start_from = ($page-1) * $limit;
        
            $sql = "SELECT COUNT(id) FROM tbl_questions";  
            $rs_result = mysqli_query($link,$sql);  
            $row = mysqli_fetch_row($rs_result);  
            $total_records = $row[0];  
            $total_pages = ceil($total_records / $limit);
            ?>
            <div class="row user-info">
                <div class="col-6">
                    Total Ques: <div id="ques_count"><?php echo $total_records; ?></div>
                </div>
                <div class="col-6>"><div id="ques_update"></div></div>
            </div> 
            <div class="row user-details">
                <div class="col-12">
                    <table class="table table-striped table-light">
                      <thead class="thead-inverse">
                        <tr>
                          <th width="200">Name</th>
                          <th>Question</th>
                          <th width="200">Asked At</th>
                          <th width="300">For Speaker?</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php		
                        $query="select * from tbl_questions order by speaker asc, answered asc, asked_at desc LIMIT $start_from, $limit";
                        $res = mysqli_query($link, $query) or die(mysqli_error($link));
                        while($data = mysqli_fetch_assoc($res))
                        {
                        ?>
                          <tr>
                            <td><?php echo $data['user_name']; ?></td>
                            <td><?php echo $data['user_question']; ?></td>
                            <td><?php 
                                $date=date_create($data['asked_at']);
                                echo date_format($date,"M d, H:i a"); ?>
                            </td>
                            <td>
                            <?php if ($data['answered'] == '0') { ?>
                            <a href="#" class="btnSpk btn btn-sm <?php if ($data['speaker'] == '0') { echo 'btn-danger'; } else { echo 'btn-success'; } ?>" onClick="updSpk('<?php echo $data['id']; ?>','<?php echo $data['speaker']; ?>')"><?php if ($data['speaker'] == '0') { echo 'Yes/No?'; } else { echo 'Cancel?'; } ?></a>
                            <?php 
                            
                            if ($data['speaker'] == '1') { ?>
                            <a href="#" class="btnSpk btn btn-sm btn-danger" onClick="updSpkAns('<?php echo $data['id']; ?>','<?php echo $data['answered']; ?>')">Mark Answered</a>
                            <?php } 
                            }
                            else
                            {
                             ?>
                            <a href="#" class="btnSpk btn btn-sm btn-danger" onClick="updSpkAns('<?php echo $data['id']; ?>','<?php echo $data['answered']; ?>')">Mark Unanswered</a>
                            <?php    
                            }
                            ?>
                            </td>
                            
                          </tr>
                      <?php			
                        }
                      ?>
                  
                    </table>  
                </div>
            </div>   
            <nav>
              <ul class="pagination pagination-sm" id="pagination">
                <?php if(!empty($total_pages)):for($i=1; $i<=$total_pages; $i++):  
                            if($i == 1):?>
                     <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php else:?>
                    <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php endif;?>
                <?php endfor;endif;?>
              </ul>
            </nav>
            <?php
        
            
        break;
        case 'logoutuser':
              $sql = "update tbl_users set logout_status='0' where id='".$_POST['userid']."'";  
              $rs_result = mysqli_query($link,$sql);  
        break;
        case 'getquesupdate':
              $sql = "SELECT COUNT(id) FROM tbl_questions";  
              $rs_result = mysqli_query($link,$sql);  
              $row = mysqli_fetch_row($rs_result);  
              $total_records = $row[0];  
              
              echo $total_records;
        break;
        
        case 'updateques':
              $newval = 0;
              if($_POST['val'] == 0)
              {
                  $newval = 1;
              }
              else
              {
                  $newval = 0;
              }
              $sql = "Update tbl_questions set show_speaker ='$newval' where id = '".$_GET['id']."'";  
              $rs_result = mysqli_query($link,$sql);  
              //$row = mysqli_fetch_row($rs_result);  
              //$total_records = $row[0];  
              
              //echo $sql;
        break;
        
        case 'updatespk':
              $newval = 0;
              if($_POST['val'] == 0)
              {
                  $newval = 1;
              }
              else
              {
                  $newval = 0;
              }
              $sql = "Update tbl_questions set speaker ='$newval', answered='0' where id = '".$_POST['ques']."'";  
              $rs_result = mysqli_query($link,$sql);  
              //$row = mysqli_fetch_row($rs_result);  
              //$total_records = $row[0];  
              
              //echo $sql;
        break;
         case 'updatespkans':
              $newval = 0;
              if($_POST['val'] == 0)
              {
                  $newval = 1;
              }
              else
              {
                  $newval = 0;
              }
              $sql = "Update tbl_questions set answered ='$newval' where id = '".$_POST['ques']."'";  
              $rs_result = mysqli_query($link,$sql);  
              //$row = mysqli_fetch_row($rs_result);  
              //$total_records = $row[0];  
              
              //echo $sql;
        break;

        case 'getpolls':
        
            if (isset($_POST["page"])) 
            { 
                $page  = $_POST["page"]; 
            }
            else { 
                $page=1; 
            }
            
            $start_from = ($page-1) * $limit;
        
            $sql = "SELECT COUNT(id) FROM tbl_polls";  
            $rs_result = mysqli_query($link,$sql);  
            $row = mysqli_fetch_row($rs_result);  
            $total_records = $row[0];  
            $total_pages = ceil($total_records / $limit);
            ?>
            <div class="row user-info">
                <div class="col-6">
                    Total Poll Questions: <div id="ques_count"><?php echo $total_records; ?></div>
                </div>
                <div class="col-6 text-right">
                    <a class="btn btn-sm btn-secondary add-poll" href="addpoll.php">Add Poll Question</a>
                </div>
                
            </div> 
            <div class="row user-details mt-1">
                <div class="col-12">
                    <table class="table table-dark table-striped">
                      <thead class="thead-inverse">
                        <tr>
                          <th>Poll Question</th>
                          <th></th>
                          <th></th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php		
                        $query="select * from tbl_polls order by poll_over, id asc LIMIT $start_from, $limit";
                        $res = mysqli_query($link, $query) or die(mysqli_error($link));
                        while($data = mysqli_fetch_assoc($res))
                        {
                        ?>
                          <tr>
                            <td><?php echo $data['poll_question']; ?></td>
                            <td width="100">
                            <?php
                                if(!$data['poll_over'])
                                {
                                    ?>
                                    <a href="#" onClick="updatePoll('<?php echo $data['id']; ?>','<?php echo $data['active']; ?>')" class="btn btn-sm <?php if ($data['active'] == '1') { echo 'btn-danger'; } else echo 'btn-success';  ?>"><?php if ($data['active'] == '1') { echo 'Deactivate'; } else echo 'Activate';  ?> Question</a>
                                    
<!--                                   <input type="checkbox" id="poll<?php echo $data['id']; ?>" name="poll<?php echo $data['id']; ?>" value="<?php echo $data['active']; ?>" onClick="updatePoll('<?php echo $data['id']; ?>')" <?php if ($data['active'] == '1') echo 'checked';  ?> <?php if ($data['poll_over'] == '1') echo 'readonly';  ?>> 
-->                                    <?php
                                }
                            ?>
                            </td>
                            <td width="100"><a href="pollresults.php?id=<?php echo $data['id']; ?>" class="btn btn-sm btn-success">View Results</a></td>
                            <td width="10"><a href="#" class="btn btn-sm btn-danger" onClick="delPoll('<?php echo $data['id']; ?>')">Delete Question</a></td>
                          </tr>
                      <?php			
                        }
                      ?>
                  
                    </table>  
                </div>
            </div>   
            <nav>
              <ul class="pagination pagination-sm" id="pagination">
                <?php if(!empty($total_pages)):for($i=1; $i<=$total_pages; $i++):  
                            if($i == 1):?>
                     <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php else:?>
                    <li onClick="update(<?php echo $i;?>)" class="page-item <?php if($_POST['page'] == $i) echo 'active'; ?>" id="<?php echo $i;?>">
                      <a class="page-link" href="#" ><?php echo $i;?></a>
                    </li>
                <?php endif;?>
                <?php endfor;endif;?>
              </ul>
            </nav>
            <?php
        
            
        break;
        
        case 'updatepoll':
            
            $newval = 0;
            if($_POST['val'] == 0)
            {
                $newval = 1;
            }
            else
            {
                $newval = 0;
            }
            
            $sql = "Update tbl_polls set active ='0'";  
            $rs_result = mysqli_query($link,$sql);
            
            if($newval){
            $sql = "Update tbl_polls set active ='$newval' where id = '".$_GET['id']."'";  
            $rs_result = mysqli_query($link,$sql); 
            }
            
            
        break;

        case 'getpollresults':
        
            $pid = $_POST['pid'];
            
            $query = 'select count(*) as c from tbl_pollanswers where poll_id = "'.$pid.'"';
            $res = mysqli_query($link, $query) or die(mysqli_error($link));
            $data = mysqli_fetch_assoc($res);
            $total_count = $data['c'];
            
            $poll = 'select * from tbl_polls where id = "'.$pid.'"';
            $res = mysqli_query($link, $poll) or die(mysqli_error($link));
            $data = mysqli_fetch_assoc($res);
            $poll_ques = $data['poll_question'];
            $poll_opt1 = $data['poll_opt1'];
            $poll_opt2 = $data['poll_opt2'];
            $poll_opt3 = $data['poll_opt3'];
            $poll_opt4 = $data['poll_opt4'];
            $corrans = $data['correct_ans'];
            
            echo '<b>'.$poll_ques.'</b><br><br>';
            //echo 'A. '.$poll_opt1.' [';
            $query = 'select count(*) as c from tbl_pollanswers where poll_id = "'.$pid.'" and poll_answer="opt1"';
            $res = mysqli_query($link, $query) or die(mysqli_error($link));
            $data = mysqli_fetch_assoc($res);
            $opt1_count = $data['c'];
            //echo $opt1_count . ']<br>';
            $opt1width = 0;
            if($total_count != '0')
            {
                $opt1width = ($opt1_count/$total_count) * 100;
            }
            $ans = '';
            if($corrans == 'opt1')
            {$ans = 'corrans'; }
            ?>
            <strong><?php echo $poll_opt1; ?></strong><span class="float-right"><?php echo $opt1_count .'('.round($opt1width,0).'%)'; ?></span>
            <div class="progress <?php echo $ans; ?>">
              <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" style="width: <?php echo $opt1width; ?>%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <?php
            
            //echo 'B. '.$poll_opt2.' [';
            $query = 'select count(*) as c from tbl_pollanswers where poll_id = "'.$pid.'" and poll_answer="opt2"';
            $res = mysqli_query($link, $query) or die(mysqli_error($link));
            $data = mysqli_fetch_assoc($res);
            $opt2_count = $data['c'];
            //echo $opt2_count . ']<br>';
            $opt2width = 0;
            if($total_count != '0')
            {
                $opt2width = ($opt2_count/$total_count) * 100;
            }
            $ans = '';
            if($corrans == 'opt2')
            {$ans = 'corrans'; }
            ?>
            <strong><?php echo $poll_opt2; ?></strong><span class="float-right"><?php echo $opt2_count .'('.round($opt2width,0).'%)'; ?></span>
            <div class="progress <?php echo $ans; ?>">
              <div class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar" style="width: <?php echo $opt2width; ?>%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <?php
            
            //echo 'C. '.$poll_opt3.' [';
            $query = 'select count(*) as c from tbl_pollanswers where poll_id = "'.$pid.'" and poll_answer="opt3"';
            $res = mysqli_query($link, $query) or die(mysqli_error($link));
            $data = mysqli_fetch_assoc($res);
            $opt3_count = $data['c'];
            //echo $opt3_count . ']<br>';
            $opt3width = 0;
            if($total_count != '0')
            {
                $opt3width = ($opt3_count/$total_count) * 100;
            }$ans = '';
            if($corrans == 'opt3')
            {$ans = 'corrans'; }
            ?>
            <strong><?php echo $poll_opt3; ?></strong><span class="float-right"><?php echo $opt3_count .'('.round($opt3width,0).'%)'; ?></span>
            <div class="progress <?php echo $ans; ?>">
              <div class="progress-bar progress-bar-striped progress-bar-animated bg-warning" role="progressbar" style="width: <?php echo $opt3width; ?>%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <?php
            //echo 'D. '.$poll_opt4.' [';
            $query = 'select count(*) as c from tbl_pollanswers where poll_id = "'.$pid.'" and poll_answer="opt4"';
            $res = mysqli_query($link, $query) or die(mysqli_error($link));
            $data = mysqli_fetch_assoc($res);
            $opt4_count = $data['c'];
            //echo $opt4_count . ']<br>';
            $opt4width = 0;
            if($total_count != '0')
            {
                $opt4width = ($opt4_count/$total_count) * 100;
            }$ans = '';
            if($corrans == 'opt4')
            {$ans = 'corrans'; }
            ?>
            <strong><?php echo $poll_opt4; ?></strong><span class="float-right"><?php echo $opt4_count .'('.round($opt4width,0).'%)'; ?></span>
            <div class="progress <?php echo $ans; ?>">
              <div class="progress-bar progress-bar-striped progress-bar-animated bg-danger" role="progressbar" style="width: <?php echo $opt4width; ?>%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <?php
            
            echo '<br><b>Total Votes: </b>'.$total_count;
            
            $query = 'select count(*) as c from tbl_pollanswers where poll_id = "'.$pid.'" and poll_answer = "'.$corrans.'"';
            $res = mysqli_query($link, $query) or die(mysqli_error($link));
            $data = mysqli_fetch_assoc($res);
            $corrans_count = $data['c'];
            
            echo '<br><b>Total Correct Answers: </b>'.$corrans_count;
            
            
        
        break;
        
        case 'getpolltimes':
            $pid = $_POST['pid'];
            $poll = 'select * from tbl_polls where id = "'.$pid.'"';
            $res = mysqli_query($link, $poll) or die(mysqli_error($link));
            $data = mysqli_fetch_assoc($res);
            $corrans = $data['correct_ans'];

            $query = 'select * from tbl_pollanswers, tbl_users where poll_id = "'.$pid.'" and poll_answer = "'.$corrans.'" and tbl_pollanswers.users_id=tbl_users.id order by poll_at asc limit 10';
            //echo $query;
            $res = mysqli_query($link, $query) or die(mysqli_error($link));
            ?>
            <table class="table table-dark table-striped">
                      <thead class="thead-inverse">
                        <tr>
                          <th>Name</th>
                          <th>Time</th>
                        </tr>
                      </thead>
                      <tbody>
            <?php
            while ($data = mysqli_fetch_assoc($res))
            {
            ?>
            <tr>
              <td><?php echo $data['name']; ?></td>
              <td><?php 
                  if($data['poll_at'] != ''){
                      $date=date_create($data['poll_at']);
                      echo date_format($date,"H:i:s"); 
                  }
                  else{
                      echo '-';
                  }
                  ?>
              </td>
            </tr>
            <?php    
            }
            ?>
            </tbody>
            </table>
            <?php
        
        
        break;
        
        case 'getpollscores':
            
            $query = 'SELECT sum(points) as total, users_id, tbl_users.name FROM `tbl_pollanswers`, tbl_users where tbl_pollanswers.users_id = tbl_users.id GROUP BY users_id order by total DESC limit 10';
            //echo $query;
            $res = mysqli_query($link, $query) or die(mysqli_error($link));
            ?>
            <table class="table table-dark table-striped">
                      <thead class="thead-inverse">
                        <tr>
                          <th>Name</th>
                          <th>Total Score</th>
                        </tr>
                      </thead>
                      <tbody>
            <?php
            while ($data = mysqli_fetch_assoc($res))
            {
            ?>
            <tr>
              <td><?php echo $data['name']; ?></td>
              <td><?php echo $data['total'];  ?>
              </td>
            </tr>
            <?php    
            }
            ?>
            </tbody>
            </table>
            <?php
        
        
        break;
        
        case 'delpoll':
              $poll = $_POST['poll'];
            
              $sql = "delete from tbl_pollanswers where poll_id='$poll'";  
              $rs_result = mysqli_query($link,$sql);  

              $sql = "delete from tbl_polls where id='$poll'";  
              $rs_result = mysqli_query($link,$sql);  
        break;
        
        case 'clear':
              $sql = "delete from tbl_pollanswers";  
              $rs_result = mysqli_query($link,$sql);  

              $sql = "delete from tbl_polls";  
              $rs_result = mysqli_query($link,$sql);  

              $sql = "delete from tbl_questions";  
              $rs_result = mysqli_query($link,$sql);  

              $sql = "delete from tbl_users";  
              $rs_result = mysqli_query($link,$sql);  
              
              echo 'success';

        break;
        
        
        
    }
    
}


?>